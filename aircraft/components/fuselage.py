# -*- coding: utf-8 -*-

import solid
import numpy as np
import bulkhead
import matplotlib.pyplot as plt

#import copy
from scipy import interpolate as interp
from scipy.integrate import cumtrapz
from CLNURBS import CLNURBS as CL
from geomdl.visualization import VisMPL
#import os
import json

plt.rcParams["figure.figsize"] = (15,10)

def dbprint(expr):
    
    print(expr + ' =', eval(expr))
    

def ruled_section(points_a, points_b):
    
    assert(np.shape(points_a) == np.shape(points_b))

    npts = np.shape(points_a)[0] - 1
    
    points = np.vstack((points_a[:-1, :], points_b[:-1, :]))
    
#    fig = plt.figure()
#    ax = fig.gca(projection='3d')
#    ax.plot(points[:,0], points[:,1], points[:,2], '.-')
##    ax.scatter(points[:,0], points[:,1], points[:,2])
#    axisEqual3D(ax)
#    plt.grid()
#    plt.draw()
#    plt.show()
#
#    fig = plt.figure()
#    ax = fig.gca()
#    ax.plot(points[:,1], points[:,2], '.-')
#    ax.axis('equal')
#    plt.grid()
#    plt.draw()
#    plt.show()
#
#    fig = plt.figure()
#    ax = fig.gca()
#    ax.plot(points[:,0], points[:,2], '.-')
#    plt.grid()
#    plt.draw()
#    plt.show()

    triangles = np.zeros((2*npts, 3), dtype=np.int)
    
    for n in range(npts-1):
        
        triangles[n] = [ n, n+1, npts + n ]
        triangles[n+npts] = [n + 1, npts + n, npts + n + 1]
    
    triangles[npts-1] = [ npts-1, 0, 2*npts - 1 ]
    triangles[2*npts-1] = [0, 2*npts - 1, npts]
    
#    fig = plt.figure()
#    ax = fig.gca(projection='3d')
#    ax.plot_trisurf(points[:,0], points[:,1], points[:,2], triangles = triangles)
##    ax.scatter(points[:,0], points[:,1], points[:,2])
#    axisEqual3D(ax)
#    plt.grid()
#    plt.draw()
#    plt.show()

    return solid.polyhedron(points=points.tolist(), faces = triangles.tolist())

def rectangular_mesh(points, p0, p1):
    
    # dimensions 0 and 1 are the rows and columns of the mesh
    # demension 2 is the xyz coordinates of the points
    
    assert(np.ndim(points) == 3)
    assert(np.shape(points)[0] > 1)
    assert(np.shape(points)[1] > 1)
    assert(np.shape(points)[2] == 3)
    
    npts = int(np.size(points)/3)
    nrows = np.shape(points)[0]
    ncols = np.shape(points)[1]
    
    X = np.vstack((p0, points.reshape((npts, 3), order='F'), p1))

    triangles = np.zeros((2*nrows*(ncols-1) + 2*nrows, 3), dtype=np.int)
    
    # front closure
    istart = 1
    
    tstart = 0
    
    for n in range(nrows-1):
        triangles[n + tstart] = [ n+istart, n+1+istart, 0]
    
    triangles[nrows-1 + tstart] = [ nrows-1+istart, 0+istart, 0]
    
    for c in range(ncols-1):
        
        istart = c*nrows + 1
        tstart = 2*c*nrows + nrows
        
        for n in range(nrows-1):
            
            triangles[n + tstart] = [ n+istart, nrows + n +istart, n+1+istart]
            triangles[n+nrows + tstart] = [n + 1+istart, nrows + n+istart, nrows + n + 1+istart]
        
        triangles[nrows-1 + tstart] = [ nrows-1+istart, 2*nrows - 1 + istart, 0+istart]
        triangles[2*nrows-1 + tstart] = [0+istart, 2*nrows - 1+istart, nrows+ istart]

    # rear closure
    istart = npts - nrows + 1
    tstart = 2*nrows*(ncols-1) + nrows
    
    for n in range(nrows-1):
        triangles[n + tstart] = [ n+istart, npts+1, n+1+istart]
   
    triangles[nrows-1 + tstart] = [ nrows-1+istart, npts+1, 0+istart]

    return solid.polyhedron(points=X.tolist(), faces = triangles.tolist())

def fuse_section(a1, b1, n1, x1, y1, z1, a2, b2, n2, x2, y2, z2):
    
    points_1 = bulkhead.super_ellipse_ish(a1, b1, n1)
    points_2 = bulkhead.super_ellipse_ish(a2, b2, n2)
    
    assert(np.shape(points_1) == np.shape(points_2))
    npts = np.shape(points_1)[0]
    
#    print('np.shape(np.full((npts,), x1))', np.shape(np.full((npts,), x1)))
#    print('np.shape(points_1[:,1])', np.shape(points_1[:,1]))
    
    points_a = np.vstack((np.full((npts,), x1), points_1[:,1] + y1, points_1[:,0] + z1)).transpose()
    points_b = np.vstack((np.full((npts,), x2), points_2[:,1] + y2, points_2[:,0] + z2)).transpose()
    
#    print('points_a =', points_a)
#    print('points_b =', points_b)
    
    return ruled_section(points_a, points_b)

def fuse_section2(a0, b0, n0, x0, y0, z0, a1, b1, n1, x1, y1, z1, a2, b2, n2, x2, y2, z2):
    
    points_0 = bulkhead.super_ellipse_ish(a0, b0, n0)[:-1, :]
    points_1 = bulkhead.super_ellipse_ish(a1, b1, n1)[:-1, :]
    points_2 = bulkhead.super_ellipse_ish(a2, b2, n2)[:-1, :]
    
    npts = np.shape(points_0)[0]
    
    points_a = np.vstack((np.full((npts,), x0), points_0[:,1] + y0, points_0[:,0] + z0)).transpose()
    points_b = np.vstack((np.full((npts,), x1), points_1[:,1] + y1, points_1[:,0] + z1)).transpose()
    points_c = np.vstack((np.full((npts,), x2), points_2[:,1] + y2, points_2[:,0] + z2)).transpose()
    
    
#    print('np.shape(np.full((npts,), x1))', np.shape(np.full((npts,), x1)))
#    print('np.shape(points_1[:,1])', np.shape(points_1[:,1]))
    
    points = np.hstack((points_a.reshape((npts, 1, 3)), points_b.reshape((npts, 1, 3)), points_c.reshape((npts, 1, 3))))
        
#    print('points_a =', points_a)
#    print('points_b =', points_b)
    
    return rectangular_mesh(points)

def __body_mesh(x, a0, a1, b, k0, k1, y, z):

    p0 = np.array([x.min(), y[0], z[0]])
    p1 = np.array([x.max(), y[-1], z[-1]])
    
    points = np.array([])
    
    for (xi, a0i, a1i, bi, k0i, k1i, yi, zi) in zip(x[1:-1], a0[1:-1], a1[1:-1], b[1:-1], k0[1:-1], k1[1:-1], y[1:-1], z[1:-1]):
        
        ai = [a0i, a1i]
        ni = [k0i, k1i]
        points_sec = bulkhead.super_ellipse_ish(ai, bi, ni)[:-1, :]
        nptsth = np.shape(points_sec)[0]
        
        points_sec1 = np.vstack((np.full((nptsth,), xi), points_sec[:,1] + yi, points_sec[:,0] + zi)).transpose()
        
        if np.size(points) == 0:
            points = points_sec1.reshape((nptsth, 1, 3))
        else:
            points = np.hstack((points, points_sec1.reshape((nptsth, 1, 3)) ))
        
    return (points, p0, p1)


# CA0 and CA1 define the lower and upper semi-major axes, respectively
# CZ defines the Z offset of the centroid
def body_meshA(CA0, CA1, CB, CK0, CK1, CY, CZ):
    nptsx = 250
   
    xmin = CA0.xmin
    xmax = CA0.xmax
    
    th = np.linspace(0, np.pi, nptsx)
    
    x = xmin + (xmax - xmin) * (1 - np.cos(th))/2.
    
    a0 = CA0(x)
    a1 = CA1(x)
    b = CB(x)
    k0 = CK0(x)
    k1 = CK1(x)
    y = CY(x)
    z = CZ(x)
 
    return(__body_mesh(x, a0, a1, b, k0, k1, y, z))

# CZ0 and CZ1 define the lower and upper profiles, respectively
# CR defines the ratio A0 = R*(Z1-Z0), A1 = (1-R)*(Z1-Z0)
def body_meshZ(CZ0, CZ1, CB, CK0, CK1, CY, CR):
    nptsx = 250
   
    xmin = CZ0.xmin
    xmax = CZ0.xmax
    
    th = np.linspace(0, np.pi, nptsx)
    
    x = xmin + (xmax - xmin) * (1 - np.cos(th))/2.

    z0 = CZ0(x)
    z1 = CZ1(x)

    a2 = z1 - z0
    r = CR(x)
    
    a0 = r*a2
    a1 = (1-r)*a2
    
    b = CB(x)
    k0 = CK0(x)
    k1 = CK1(x)
    y = CY(x)

    z = z0 + a0
        
    return (__body_mesh(x, a0, a1, b, k0, k1, y, z))

# CZ0, CZ1, and CZ2 define the lower, midline, and upper profiles, respectively
def body_meshZ2(CZ0, CZ1, CZ2, CB, CK0, CK1, CY):
    nptsx = 250
   
    xmin = CZ0.xmin
    xmax = CZ0.xmax
    
    th = np.linspace(0, np.pi, nptsx)
    
    x = xmin + (xmax - xmin) * (1 - np.cos(th))/2.

    z0 = CZ0(x)
    z1 = CZ2(x)
    zm = CZ1(x)

    a0 = zm - z0
    a1 = z1 - zm
    
    b = CB(x)
    k0 = CK0(x)
    k1 = CK1(x)
    y = CY(x)

    z = zm
        
    return (__body_mesh(x, a0, a1, b, k0, k1, y, z))


def makeCL(d, field, debug):
    
    xyw = np.array(d[field])
    C = CL(xw = xyw[:,0]*xyw[:,2], yw = xyw[:,1]*xyw[:,2], w = xyw[:,2])
    if debug:
        vis_config = VisMPL.VisConfig(figure_size=[20, 15])
        vis_comp = VisMPL.VisCurve2D(config=vis_config)
        C.curve.vis = vis_comp
        C.curve.render()

    return C
    

def bodyOML(filename, debug=False):

    with open(filename, 'r') as myfile:
        d = json.load(myfile)

        CB = makeCL(d, 'B', debug)
        CK0 = makeCL(d, 'K0', debug)
        CK1 = makeCL(d, 'K1', debug)
        CY = makeCL(d, 'Y', debug)

        try:
            CA0 = makeCL(d, 'A0', debug)
            CA1 = makeCL(d, 'A1', debug)
            CZ = makeCL(d, 'Z', debug)
                
            points, p0, p1 = body_meshA(CA0, CA1, CB, CK0, CK1, CY, CZ)
        
        except:

            CZ0 = makeCL(d, 'Z0', debug)
            CZ1 = makeCL(d, 'Z1', debug)
            
            try:
                CR = makeCL(d, 'R', debug)
                points, p0, p1 = body_meshZ(CZ0, CZ1, CB, CK0, CK1, CY, CR)

            except:
                CZ2 = makeCL(d, 'Z2', debug)
                points, p0, p1 = body_meshZ2(CZ0, CZ1, CZ2, CB, CK0, CK1, CY)
                                            
        return rectangular_mesh(points, p0, p1)


def axisEqual3D(ax):
    extents = np.array([getattr(ax, 'get_{}lim'.format(dim))() for dim in 'xyz'])
    sz = extents[:,1] - extents[:,0]
    centers = np.mean(extents, axis=1)
    maxsize = max(abs(sz))
    r = maxsize/2
    for ctr, dim in zip(centers, 'xyz'):
        getattr(ax, 'set_{}lim'.format(dim))(ctr - r, ctr + r)


# x0 - initial X
# y0 - initial y
# theta0 - initial atan2(dy,dx)
# s - path length knot points
# K - curvature knot points
def cubic_primative(x0, y0, theta0, s, K):

    npts = 1000
    
    # piecewise linear mapping from s to K
    
    s = np.asarray(s, dtype=np.float).copy()
    K = np.asarray(K, dtype=np.float).copy()
    
    Bks = interp.make_interp_spline(x=s, y=K, k=1, bc_type=None)
    
    svec = np.linspace(s[0], s[-1], npts)
    
    theta = np.array([Bks.integrate(0., s) for s in svec])

    deltheta_total = 0.5*np.sum((K[0:-1] + K[1:])/np.diff(s))
    
    print('deltheta_total =', deltheta_total)
    
    ds = np.median(np.diff(svec))
    
    x = x0 + np.hstack((0., cumtrapz(ds*np.cos(theta))))
    y = y0 + np.hstack((0., cumtrapz(ds*np.sin(theta))))
    
    x1 = interp.interp1d(svec, x, s[1])
    
    
    fig1 = plt.figure()
    ax1 = fig1.gca()
    ax1.plot(svec, Bks(svec), '-')

    plt.ylabel('K')
    plt.xlabel('s')
#    plt.axis('equal')
    plt.grid()

    fig2 = plt.figure()
    ax2 = fig2.gca()
    ax2.plot(svec, theta, '-')

    plt.ylabel('theta')
    plt.xlabel('s')
#    plt.axis('equal')
    plt.grid()

    fig3 = plt.figure()
    ax3 = fig3.gca()
    ax3.plot(svec, x, '-')

    plt.ylabel('x')
    plt.xlabel('s')
#    plt.axis('equal')
    plt.grid()

    fig4 = plt.figure()
    ax4 = fig4.gca()
    ax4.plot(svec, y, '-')

    plt.ylabel('y')
    plt.xlabel('s')
#    plt.axis('equal')
    plt.grid()

    fig5 = plt.figure()
    ax5 = fig5.gca()
    ax5.plot(x, y, '-')

    plt.ylabel('y')
    plt.xlabel('x')
#    plt.axis('equal')
    plt.grid()

    plt.draw()
    plt.show()

def test_spline():
    
    npts = 1000
    smax = 0.7
    scale = 1.0    
    tht0 = np.pi/2
    tht1 = -7.5*np.pi/180.
    K0 = -10.
    K1 = 0.

    ## no humans allowed below here
    s = np.array([0., smax])
    x = np.array([0., 1.0])
    y = np.array([0., 0.0])
    k = 5
    
    bcx = ([(1,np.cos(tht0)), (2,-K0*np.sin(tht0))], [(1, np.cos(tht1)), (2, -K1*np.sin(tht1))])
    bcy = ([(1,np.sin(tht0)), (2,+K0*np.cos(tht0))], [(1, np.sin(tht1)), (2, +K1*np.cos(tht1))])
    
    bx = interp.make_interp_spline(x=s, y=x, k=k, bc_type=bcx)
    by = interp.make_interp_spline(x=s, y=y, k=k, bc_type=bcy)
    
    bdx  = bx.derivative(1)
    bddx = bx.derivative(2)
    bdy  = by.derivative(1)
    bddy = by.derivative(2)
    
    print('bx(s[0])=', bx(s[0]), '                bx(s[1])=', bx(s[1]))
    print('bdx(s[0])=', bdx(s[0]), '              bdx(s[1])=', bdx(s[1]))
    print('bddx(s[0])=', bddx(s[0]), '            bddx(s[1])=', bddx(s[1]))
    print('by(s[0])=', by(s[0]), '                by(s[1])=', by(s[1]))
    print('bdy(s[0])=', bdy(s[0]), '              bdy(s[1])=', bdy(s[1]))
    print('bddy(s[0])=', bddy(s[0]), '            bddy(s[1])=', bddy(s[1]))

    sv = np.linspace(s[0], s[1], npts)

    xv   = bx(sv)
    xdv  = bdx(sv)
    xddv = bddx(sv)
    yv   = by(sv)
    ydv  = bdy(sv)
    yddv = bddy(sv)
    
#    print('sv = ', sv)
#    print('xv = ', xv)
#    print('yv = ', yv)
#    print('xdv = ', xdv)
#    print('ydv = ', ydv)
#    print('xddv = ', xddv)
#    print('yddv = ', yddv)


    sdv = np.sqrt(xdv*xdv + ydv*ydv)
    tht = np.arctan2(ydv/sdv, xdv/sdv)

#    num = (xdv*yddv - ydv*xddv)
#    den = (xdv*xdv + ydv*ydv)**1.5
#    print('den = ', den)
    
#    print('num = ', num)
    
#    K = num/den
    K = (xdv*yddv - ydv*xddv)/((xdv*xdv + ydv*ydv)**1.5)
    
    fig = plt.figure()
    ax = fig.gca()
    ax.plot(xv*scale, yv*scale, '-')
    ax.plot(x*scale, y*scale, '.')
    plt.ylabel('yv')
    plt.xlabel('xv')
    ax.axis('equal')
    plt.grid()
    plt.draw()
    plt.show()

#    fig = plt.figure()
#    ax = fig.gca()
#    ax.plot(sv, xv, '-')
#    ax.plot(sv, yv, '-')
#    plt.ylabel('xv, yv')
#    plt.xlabel('s')
#    plt.grid()
#    plt.draw()
#    plt.show()
    
#    fig = plt.figure()
#    ax = fig.gca()
#    ax.plot(sv, xdv, '-')
#    ax.plot(sv, ydv, '-')
#    ax.plot(sv, sdv, '-')
#    plt.ylabel('xdv, ydv')
#    plt.xlabel('s')
#    plt.grid()
#    plt.draw()
#    plt.show()

#    fig = plt.figure()
#    ax = fig.gca()
#    ax.plot(sv, xddv, '-')
#    ax.plot(sv, yddv, '-')
#    plt.ylabel('xddv, yddv')
#    plt.xlabel('s')
#    plt.grid()
#    plt.draw()
#    plt.show()

    fig = plt.figure()
    ax = fig.gca()
    ax.plot(sv, tht*180./np.pi, '-')
    plt.grid()
    plt.draw()
    plt.ylabel('theta (deg)')
    plt.xlabel('s')
    plt.show()
    
    fig = plt.figure()
    ax = fig.gca()
    ax.plot(sv, K, '-')
    plt.grid()
    plt.draw()
    plt.ylabel('K')
    plt.xlabel('s')
    plt.show()



    
 