#!/usr/bin/env python

#################################################################################
# This file is part of airfoil-utils.
#
# airfoil-utils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# airfoil-utils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with airfoil-utils.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
#
# Script to convert airfoil coordinate .dat files into SVG format, with minor
# additional attributes to make them easy to view in Inkscape.
#
# Currently, this script has only been tested on files from:
#
# http://m-selig.ae.illinois.edu/ads/coord_database.html
#
# Author: John Casey (jdcasey@commonjava.org)
#
#################################################################################

import re
import requests
import numpy as np
from scipy.interpolate import interp1d
from scipy.interpolate import UnivariateSpline

UIUC_URL_FMT='http://m-selig.ae.illinois.edu/ads/coord/%s'

# return a 1 mm chord length numpy array of coordinates for the specified
# airfoil
def Foil(*, dat_filename=None, uiuc_filename=None, url=None, interpolate=True, smoothing=True):

    if dat_filename is not None:
        with open(dat_filename) as f:
            lines = [l.strip() for l in f.readlines()]
    elif uiuc_filename is not None:
        if not uiuc_filename.endswith(".dat"):
            uiuc_filename = uiuc_filename + ".dat"
        url = UIUC_URL_FMT % (uiuc_filename)
        lines = __poly_url(url)
    elif url is not None:
        lines = __poly_url(url)

    foil = __read_surfaces(lines)

    if interpolate is True:
        foil = __foil_interpolate(foil, smoothing=smoothing)
    elif interpolate > 0:
        foil = __foil_interpolate(foil, npts=interpolate, smoothing=smoothing)

    if not (foil[0,0] == foil[-1,0] and
        foil[0,1] == foil[-1,1]):
    
        # close the curve
        
        last_upper = foil[0, :]
        last_lower = foil[-1, :]
    
        foil = np.concatenate(([(last_upper+last_lower)/2.], foil, [(last_upper+last_lower)/2.]))

    return foil

def Upper(points):
    
    ii = isUpper(points)
    p = points[ii,:]
    jj = np.argsort(p[:,0])
    
    return (p[jj,:])

def Lower(points):
    
    ii = isLower(points)
    p = points[ii,:]
    jj = np.argsort(p[:,0])
    
    return (p[jj,:])

def Camber(points):
    
    _, c = __tc_integ(points)

    return c
    
def Thickness(points):

    t, _ = __tc_integ(points)
    
    return t
    
# integral solution of thickness and camber line
def __tc_integ(points):
    
    u = Upper(points)
    l = Lower(points)
    
    dxuds, dyuds = dds(u)
    dxlds, dylds = dds(l)
    
    # TODO: integrate from tailing edge to leading edge
    # at each point, step forward one ds increment, finding the largest circle 
    # that is tangent to the upper and lower surface
    # the circle center is on the camberline, and the radius is the thickness
    # Alternatively investigate Voronoi diagram solution see:
    # https://stackoverflow.com/a/14073485
    
    return None, None
    
def dds(points):
    
    # calculate the linear length at each node
    s = np.hstack(([0.], np.cumsum(np.hypot(np.diff(points[:,0]), np.diff(points[:,1])))))
    
    ## first derivative
    dxds_0 = (points[1,0] - points[0,0])/(s[1]-s[0])
    dxds_trap = (points[2:,0] - points[:-2,0])/(s[2:]-s[:-2])
    dxds_1 = (points[-1,0] - points[-2,0])/(s[-1]-s[-2])
    
    dyds_0 = (points[1,1] - points[0,1])/(s[1]-s[0])
    dyds_trap = (points[2:,1] - points[:-2,1])/(s[2:]-s[:-2])
    dyds_1 = (points[-1,1] - points[-2,1])/(s[-1]-s[-2])
    
    dxds=np.hstack(([dxds_0], dxds_trap, [dxds_1]))
    dyds=np.hstack(([dyds_0], dyds_trap, [dyds_1]))
    
    ngrad = np.hypot(dxds, dyds)
    
    dxds=dxds/ngrad
    dyds=dyds/ngrad
    
    return (dxds, dyds)
    
# assumes Selig points ordering
# i.e. trailing edge, upper, around the leading edge, lower back to trailing edge
def isUpper(points):
    
    dxds, _ = dds(points)
    
    return dxds < 0.

# assumes Selig points ordering
# i.e. trailing edge, upper, around the leading edge, lower back to trailing edge
def isLower(points):
    
    dxds, _ = dds(points)
    
    ii = np.full_like(points[:,0], False)
    
    ii = dxds > 0.
    
    return dxds > 0.

def __poly_url(url):

    r = requests.get(url)

    if r.status_code == requests.codes.ok:
        lines = r.text.splitlines()
    else:
        raise("Error: %d response from %s" % (r.status_code, url))

    return lines

def __read_surfaces(lines):
    
    # Selig format has a single text header line and then starts the 
    # coordinates on the second row, upper trailing edge first
    
    rtest = '\s*[+-]?([.0-9])+\s*[+-]?([.0-9])+\s*'
    
    if (re.match(rtest, lines[0]) is None) \
        and (re.match(rtest, lines[1]) is not None) \
        and (re.match(rtest, lines[2]) is not None) \
        and (re.match(rtest, lines[3]) is not None) :
        
        #print('Selig Format')
        foil = __read_surfaces_selig(lines)

    # Lednicer format has a text header line, an array dimensions line, a 
    # blank line, then starts with the coordinates, upper leading edge first
    elif (re.match(rtest, lines[0]) is None) \
        and (re.match(rtest, lines[1]) is not None) \
        and (re.match(rtest, lines[2]) is None) \
        and (re.match(rtest, lines[3]) is not None) :

        #print('Lednicer Format')
        foil = __read_surfaces_lednicer(lines)

    else:
        foil = None

    foil = np.array(foil)
    
    return foil

def __read_surfaces_selig(lines):

    surfaces = __line_parse(lines[1:])
    
    # Now, create an aggregated surface.
    # Start with a copy of the first original surface, intact.
    aggregated_surface = list(surfaces[0])
    
    if len(surfaces) > 1:
        # Reverse the second set of coordinates and append them to the first.
        # This traces the second line (second surface) backward, starting from a 
        # point where the two surfaces connect on the trailing edge
        for coord in reversed(surfaces[1]):
            aggregated_surface.append(coord)

    return aggregated_surface

def __read_surfaces_lednicer(lines):

    surfaces = __line_parse(lines[3:])
    
    # Now, create an aggregated surface.
    # Reverse the first set of coordinates to go trailing edge to leading edge
    aggregated_surface = list(reversed(surfaces[0]))
    
    if len(surfaces) > 1:
        # Append the second set of coordinates to the first.
        # This traces the second line (second surface) forward, from 
        # the leading edge to the trailing edge
        for coord in surfaces[1]:
            aggregated_surface.append(coord)
    
    return aggregated_surface

def __line_parse(lines):

    surfaces = []
    current_surface = []
    
    # Iterate through each line. If the line is blank
    # start a new coordinate set (surface).
    # For each line in the file, parse out a X, Y coordinate, amplified by the FACTOR
    # and translated to the XLATE, YLATE offsets.
    # Store each X,Y coordinate as a list of floats, in case we have to do further massaging.
    for line in lines:
        if len(line) < 3:
            surfaces.append(current_surface)
            current_surface = []
        else:
            xy = [float(str(s)) for s in re.split("\s", line) if len(str(s)) > 0]
            # print xy
            if len(xy) > 1:
                current_surface.append(xy)
    
    # Cleanup; add the last coordinate set we were working on to the list of coordinate sets.
    surfaces.append(current_surface)

    return surfaces    

# perform a continuous curvature interpolation to increase the points density
# on an input airfoil curve
def __foil_interpolate(points, *, npts=1000, smoothing=False):

    # the Joukowski conformal mapping is given by A = z + 1/z if z is the 
    # complex coordinate in the cylinder plane and A is the complex coordinate
    # in the aifoil plane
        
    # this isn't actually the thickness, but it's close enough for what we need
    thickness = np.max(points[:,1]) - np.min(points[:,1])
    
    # filter repeated points
    ii = np.diff(points[:,0]) == 0.
    
    points = np.delete(points, ii.nonzero(), axis=0)
    
    ## create the interpolation basis, theta
    isupper = isUpper(points)
    islower = np.logical_not(isupper)
    
    theta = 0*points[:,0]
    theta[isupper] = np.arccos(2*points[isupper,0] - 1)
    theta[islower] = -np.arccos(2*points[islower,0] - 1)
    theta = np.unwrap(theta)

    ## reference x points, formed from a Joukowski circle projection
    r = 1-thickness

    xref = r/2*np.cos(theta) + 1 - r/2
    dx = points[:,0] - xref
    dy = points[:,1]

    ## support either cubic or smoothing spline methods for interpolation
    if smoothing is True:
        dx_interp = UnivariateSpline(theta, dx, s=1e-6)
        dy_interp = UnivariateSpline(theta, dy, s=1e-6)
    elif smoothing is False:
        dx_interp = interp1d(theta, dx, kind='cubic')
        dy_interp = interp1d(theta, dy, kind='cubic')
    else:
        dx_interp = UnivariateSpline(theta, dx, s=smoothing)
        dy_interp = UnivariateSpline(theta, dy, s=smoothing)

    ## generate the interpolated foil
    theta_fine = np.linspace(0., 2*np.pi, npts).transpose()

    dx_fine = dx_interp(theta_fine)
    dy_fine = dy_interp(theta_fine)
    xref_fine = r/2*np.cos(theta_fine) + 1 - r/2

    x_fine = dx_fine + xref_fine
    y_fine = dy_fine
    
    return np.vstack((x_fine, y_fine)).transpose()
