# -*- coding: utf-8 -*-
"""
Created on Sat Mar 17 17:44:42 2018

@author: Alex
"""

import numpy as np
import csv
from scipy import interpolate as interp
import scipy.optimize as opt

class CLJouk:

    def __init__(self, s=None, x=None, y=None, theta=None, K=None):
    
        if s is not None:
            self.set(s, x, y, theta, K)

    def read(self, csv_filename):
        
        with open(csv_filename) as f:
            
            csv.register_dialect('myDialect',
                                    quotechar='"', 
                                  delimiter=',',
                                  skipinitialspace=True, 
                                  lineterminator='\n', 
                                  strict=False)
            
            reader = csv.DictReader(f, dialect = 'myDialect')
            
            dsnorm = []
            x = []
            y = []
            theta = []
            K = []
            
            for row in reader:
                
                dsnorm.append(row['dsnorm'])
                x.append(row['x'])
                y.append(row['y'])
                theta.append(row['theta'])
                K.append(row['K'])
                
            dsnorm = np.array(dsnorm, dtype=np.float)
            x = np.array(x, dtype=np.float)
            y = np.array(y, dtype=np.float)
            theta = np.array(theta, dtype=np.float)*np.pi/180.
            K = np.array(K, dtype=np.float)
            
            print(dsnorm, x, y, theta, K)
            
            self.set(dsnorm, x, y, theta, K)

    def __f_phi(z, x, y, xcenter, rref):
        
        n = int(np.size(z)/2)
        
        phi = z[:n]
        r = z[n:]

        dx = x - (xcenter - (rref + r)*np.cos(phi))
        dy = y - r*np.sin(phi)
        
        return np.sum(dx**2. + dy**2.)

    def set(self, dsnorm, x, y, theta, K):
        
        npts = 1000
        korder = 5
        
        rfac0 = 0.5
        rfac1 = 0.1
        
        self.x0 = np.asarray(x).copy()
        self.y0 = np.asarray(y).copy()
        self.theta0 = np.asarray(theta).copy()
        self.K0 = np.asarray(K).copy()

        n = np.size(self.x0)
        
        xmin = self.x0.min()
        xmax = self.x0.max()
        delx = xmax - xmin
        rfull = delx/2.
        
        self.xcenter = (xmin + rfull*rfac0 + xmax - rfull*rfac1)/2.
        self.rref = (xmax - rfull*rfac1 - (xmin + rfull*rfac0) )/2.

        self.Brp = []
        self.p = [0]
        
        phi0 = np.arccos((self.xcenter - self.x0)/(self.xcenter - self.x0).max())
        r0 = self.y0

        print('phi0 =', phi0)
        print('r0 =', r0)
        
        z0 = np.hstack((phi0, r0))
        
        z = opt.fmin(CLJouk.__f_phi, z0, args = (self.x0, self.y0, self.xcenter, self.rref), ftol = 1e-10, maxiter = 10000)
        
        self.phi0 = z[:n]
        self.r0 = z[n:]

        print('phi0 =', self.phi0)
        print('r0 =', self.r0)

        self.phi0[0] = 0.
        self.phi0[-1] = np.pi
        
        self.r0[0] = (self.xcenter - self.rref) - xmin
        self.r0[-1] = xmax - (self.xcenter + self.rref)

        print('phi0 =', self.phi0)
        print('r0 =', self.r0)

        # positive gamma, radius is increasing, negative gamma, decreasing
        gamma = self.theta0 - (np.pi/2. - self.phi0)
        
        print('gamma =', gamma)

        drdphi = self.r0*np.tan(gamma) + self.rref*np.sin(self.phi0)*(np.cos(self.phi0) + np.sin(self.phi0)*np.tan(gamma))
#        dxdphi = (self.rref + self.r0)*np.sin(self.phi0) - drdphi*np.cos(self.phi0)
#        dydphi = self.r0*np.cos(self.phi0) + drdphi*np.sin(self.phi0)
        
        print('drdphi =', drdphi)
        
        d2rdphi2 = self.K0 * self.r0*np.cos(gamma)
        
        print('d2rdphi2 = ', d2rdphi2)

#        dsdphi = np.hypot(drdphi, self.r0)

        print('self.phi0 =', self.phi0)
        print('self.r0 =', self.r0)
        
        for i in range(np.size(self.x0)-1):

            phii = [self.phi0[i], self.phi0[i+1]]
            ri = [self.r0[i], self.r0[i+1]]

            bcr = ([(1, drdphi[i]), (2, d2rdphi2[i])], [(1, drdphi[i+1]), (2, d2rdphi2[i+1])])
#            bcr = ([(1, drdphi[i])], [(1, drdphi[i+1])])
            self.Brp.append(interp.make_interp_spline(x=phii, y=ri, k=korder, bc_type=bcr))

        snorm = np.linspace(0., 1.0, npts)
        
        xs, ys = self.__call__(snorm)

        try:
            self.Byx = interp.PchipInterpolator(x=xs, y=ys)
            self.Bsx = interp.PchipInterpolator(x=xs, y=snorm)
        except:
            self.Byx = None
            self.Bsx = None

    def __call__(self, phi):

        phi = np.asarray(phi).copy()
        r = np.full(np.shape(phi), 0.)
        x = np.full(np.shape(phi), 0.)
        y = np.full(np.shape(phi), 0.)
        
        for i in range(np.size(self.Brp)):
            
            ii = np.logical_and( phi >= self.phi0[i], phi <= self.phi0[i+1] )

            r[ii] = self.Brp[i](phi[ii])
            x[ii] = self.xcenter - (self.rref + r[ii])*np.cos(phi[ii])
            y[ii] = r[ii]*np.sin(phi[ii])
            
        return x, y
    
    def y_x(self, x):
        
        return self.Byx(x)

    def r(self, phi):

        phi = np.asarray(phi).copy()
        r = np.full(np.shape(phi), 0.)

        for i in range(np.size(self.Brp)):

            ii = np.logical_and( phi >= self.phi0[i], phi <= self.phi0[i+1] )

            r[ii] = self.Brp[i](phi[ii])
        
        return r
    
    def theta(self, snorm):

        s = np.asarray(snorm*self.s[-1]).copy()
        xd = np.full(np.shape(s), 0.)        
        yd = np.full(np.shape(s), 0.)        

        for i in range(np.size(self.Bxs)):

            ii = np.logical_and( s >= self.s[i], s <= self.s[i+1] )

            xd[ii] = self.Bxs[i](s[ii], 1)
            yd[ii] = self.Bys[i](s[ii], 1)
        
        return np.arctan2(yd, xd)

    def K(self, snorm):

        s = np.asarray(snorm*self.s[-1]).copy()
        xd = np.full(np.shape(s), 0.)        
        yd = np.full(np.shape(s), 0.)        
        xdd = np.full(np.shape(s), 0.)        
        ydd = np.full(np.shape(s), 0.)        

        for i in range(np.size(self.Bxs)):

            ii = np.logical_and( s >= self.s[i], s <= self.s[i+1] )

            xd[ii] = self.Bxs[i](s[ii], 1)
            yd[ii] = self.Bys[i](s[ii], 1)
            xdd[ii] = self.Bxs[i](s[ii], 2)
            ydd[ii] = self.Bys[i](s[ii], 2)
        
        return (xd*ydd - yd*xdd)/((xd*xd + yd*yd)**1.5)
