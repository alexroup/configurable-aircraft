# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 21:15:24 2018

@author: Alex
"""

import solid
import numpy as np

def make(b2, cr, ct, cflap, xtle, thickness, web_depth, num_ribs=0, rib_width=0., r=0., single_surface=True, root_v=True, output_filename='surf.scad'):

    xtte = xtle+ct
    x_max = cr + ct + np.abs(xtle)
    
    points = [[0,0], [cr + thickness, 0], [xtte + thickness, b2], [xtle, b2], [0,0]]
    
    p0 = solid.polygon(points)
    
    if root_v:
        p = p0 - solid.polygon([[cr-cflap, 0], [x_max + cr-cflap, 0], [x_max + cr-cflap, x_max + thickness], [cr-cflap, thickness], [cr-cflap, 0]])
    else:
        p = p0
    
    p_ribs = ribs(b2, cr, ct, xtle, web_depth, num_ribs)

    points_surf = [[0,0], [cr - cflap, 0], [xtte - cflap, b2], [xtle, b2], [0,0]]
    p_surf = solid.polygon(points_surf)

    points_flap = [[cr - cflap + thickness, 0], [cr + thickness, 0], [xtte + thickness, b2], [xtte -cflap + thickness, b2], [cr - cflap + thickness,0]]
    p_flap = solid.polygon(points_flap)
    
    points_bevel = [[0, 0], [0, -thickness], [thickness, -thickness], [thickness, 0], [2*thickness, thickness], [2*thickness, 2*thickness],
               [-thickness/2, 2*thickness], [-thickness/2, thickness],
               [0, 0]]

#    points_bevel = [[cr - cflap, 0], [cr - cflap + thickness, thickness], [cr - cflap + thickness, 2*thickness],
#               [cr - cflap - thickness/2, 2*thickness], [cr - cflap - thickness/2, thickness],
#               [cr - cflap, 0]]
    
    s_bevel = solid.translate([cr-cflap, 0, 0]) (
            solid.rotate(v=[0,0,1], a=180/np.pi*np.arctan2(xtte-cr, b2)) (
                solid.rotate(v=[1,0,0], a=90) (
                    solid.linear_extrude(4*np.hypot(xtle, b2), center=True) (
                            solid.polygon(points_bevel)
                        )
                    )
                )
            )
    
    s_bevel2 = solid.mirror([0,1,0])(
                s_bevel
            )

    p2 = p + solid.mirror([0,1,0])(p)
    p_ribs = p_ribs + solid.mirror([0,1,0])(p_ribs)
    p_surf = p_surf + solid.mirror([0,1,0])(p_surf)
    p_flap = p_flap + solid.mirror([0,1,0])(p_flap)

    p_round = round_polygon(r = r, p = p2)

    p_surf = p_round * p_surf
    p_flap = p_round * p_flap
    
    p_interior_surf = solid.offset(r = -web_depth) (p_surf)
    p_interior_flap = solid.offset(r = -web_depth) (p_flap)
    
    p_interior_surf = p_interior_surf - p_ribs
    p_interior_flap = p_interior_flap - p_ribs
    p_interior_surf = round_polygon(r = web_depth/2, p = p_interior_surf)
    p_interior_flap = round_polygon(r = web_depth/2, p = p_interior_flap)
    
    p_all = (p_surf - p_interior_surf) + (p_flap - p_interior_flap)
    
    s = solid.linear_extrude(thickness)(p_all) - s_bevel2

    points_cut = [[-2*x_max, 0.], [2*x_max, 0.], [2*x_max, -2*b2], [-2*x_max, -2*b2], [-2*x_max, 0.]]
    
    s_cut = solid.linear_extrude(height=3*thickness, center=True) (solid.polygon(points_cut))
    s = s - s_cut
    
    if not single_surface:
        s = s + solid.mirror([0,1,0]) (s)
    
    solid.scad_render_to_file(s, output_filename, file_header='$fn = 50;')


def ribs(b2, cr, ct, xtle, web_depth, num_ribs):

    w = b2 - web_depth*1.5
    
    dw = w/(num_ribs+1)
    
    x_max = cr + ct + np.abs(xtle)
    
    p = solid.union()
    for k in range(num_ribs+1):
        
        y_center = k*dw + web_depth*0.5
        
        points = [[-2*x_max, -web_depth/2 + y_center], [-2*x_max, +web_depth/2 + y_center], 
                  [2*x_max, +web_depth/2 + y_center], [2*x_max, -web_depth/2 + y_center],
                  [-2*x_max, -web_depth/2 + y_center]]

        p = p + solid.polygon(points)
        
    return p

def round_polygon(r, p):

    return solid.offset(r=r) (solid.offset(r=-r)(p))
