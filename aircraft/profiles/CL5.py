# -*- coding: utf-8 -*-
"""
Created on Sat Mar 17 17:41:45 2018

@author: Alex
"""

import numpy as np
import csv
from scipy import interpolate as interp
from scipy.integrate import cumtrapz
import scipy.optimize as opt

class CL5:

    def __init__(self, s=None, x=None, y=None, theta=None, K=None):
    
        if s is not None:
            self.set(s, x, y, theta, K)

    def read(self, csv_filename):
        
        with open(csv_filename) as f:
            
            csv.register_dialect('myDialect',
                                    quotechar='"', 
                                  delimiter=',',
                                  skipinitialspace=True, 
                                  lineterminator='\n', 
                                  strict=False)
            
            reader = csv.DictReader(f, dialect = 'myDialect')
            
            dsnorm = []
            x = []
            y = []
            theta = []
            K = []
            
            for row in reader:
                
                dsnorm.append(row['dsnorm'])
                x.append(row['x'])
                y.append(row['y'])
                theta.append(row['theta'])
                K.append(row['K'])
                
            dsnorm = np.array(dsnorm, dtype=np.float)
            x = np.array(x, dtype=np.float)
            y = np.array(y, dtype=np.float)
            theta = np.array(theta, dtype=np.float)*np.pi/180.
            K = np.array(K, dtype=np.float)
            
            print(dsnorm, x, y, theta, K)
            
            self.set(dsnorm, x, y, theta, K)

    def set(self, dsnorm, x, y, theta, K):
        
        npts = 1000

        self.x0 = np.asarray(x).copy()
        self.y0 = np.asarray(y).copy()
        self.theta0 = np.asarray(theta).copy()
        self.K0 = np.asarray(K).copy()

        self.Byt = []
        self.Bxt = []
        self.Bts = []
        self.t = [0]
        self.s = [0]
        
        for i in range(np.size(self.x0)-1):

            xi = [x[i], x[i+1]]
            yi = [y[i], y[i+1]]

            dsmin = np.hypot(np.diff(xi), np.diff(yi))[0]
            dsmax = (np.abs(np.diff(xi)) + np.abs(np.diff(yi)))[0]
            
            ds = dsnorm[i+1]*(dsmin + dsmax)/2.
            
            si = [self.s[i], self.s[i] + ds]

            thetai = [theta[i], theta[i+1]]
            Ki = [K[i], K[i+1]]
            
            Bxt, Byt, Bts, dt = CL5.__solve_s(si, xi, yi, thetai, Ki)
            
            # mapping from t to x
            self.Bxt.append(Bxt)

            # mapping from t to y
            self.Byt.append(Byt)

            # mapping from s to t
            self.Bts.append(Bts)
            
            # t knot values
            self.t.append(self.t[i] + dt)

            # s knot values
            self.s.append(self.s[i] + ds)

        snorm = np.linspace(0., 1.0, npts)
        
        xs, ys = self.__call__(snorm)

        try:
            self.Byx = interp.PchipInterpolator(x=xs, y=ys)
            self.Bsx = interp.PchipInterpolator(x=xs, y=snorm)
        except:
            self.Byx = None
            self.Bsx = None

    def __setSplines(s0, dt, x, y, theta, K):
        
        korder = 5
        npts = 1000
        
        # boundary conditions            
        bcx = ([(1,np.cos(theta[0])), (2,-K[0]*np.sin(theta[0]))], [(1, np.cos(theta[1])), (2, -K[1]*np.sin(theta[1]))])
        bcy = ([(1,np.sin(theta[0])), (2,+K[0]*np.cos(theta[0]))], [(1, np.sin(theta[1])), (2, +K[1]*np.cos(theta[1]))])
        
        tvec = np.linspace(0., dt, npts)

        # mapping from t to x
        Bxt = interp.make_interp_spline(x=[0., dt], y=x, k=korder, bc_type=bcx)

        # mapping from t to y
        Byt = interp.make_interp_spline(x=[0., dt], y=y, k=korder, bc_type=bcy)
       
        dxdtvec = Bxt(tvec, 1)
        dydtvec = Byt(tvec, 1)
        dsdtvec = np.sqrt(dxdtvec*dxdtvec + dydtvec*dydtvec)
        
        # path length vector
        svec = s0 + np.hstack((0., cumtrapz(dsdtvec, tvec)))

        Bts = interp.CubicSpline(x=svec, y=tvec)
        
        return (Bxt, Byt, Bts, svec[-1])

    def __f(s, dt, x, y, theta, K):
        
        _, _, _, sret = CL5.__setSplines(s[0], dt, x, y, theta, K)
        
        return (sret - s[1])**2.
    
    def __solve_s(s, x, y, theta, K):
        
        f = lambda dtx: CL5.__f(s, dtx, x, y, theta, K)
        
        tmin = 0.1*np.hypot(np.diff(x), np.diff(y))[0]
        tmax = 5*(np.diff(x) + np.diff(y))[0]
        
        res = opt.minimize_scalar(f, bounds = [tmin, tmax], method='bounded', 
                  options={'disp': 1, 'maxiter': 500, 'xatol': 1e-05})
        
        dt = res.x
        
#        print(res.message)
        
        assert(res.success==True)
        
        Bxt, Byt, Bts, sret = CL5.__setSplines(s[0], dt, x, y, theta, K)
        
        assert(res.fun < 1e-5)
        
#        print('ds =', ds, 'dsret =', sret - s[0], 'bounds =', [0.3*ds, 3*ds], 'dt =', dt, 's[1] =', s[1], 'sret =', sret, 'dt =', dt, 'fun =', res.fun, 'nfev =', res.nfev)
        
        return Bxt, Byt, Bts, dt

    def __call__(self, snorm):

        s = np.asarray(snorm*self.s[-1]).copy()
        x = np.full(np.shape(s), 0.)        
        y = np.full(np.shape(s), 0.)        
        
        for i in range(np.size(self.Bxt)):
            
            ii = np.logical_and( s >= self.s[i], s <= self.s[i+1] )

            t = self.Bts[i](s[ii])
          
            x[ii] = self.Bxt[i](t)
            y[ii] = self.Byt[i](t)
            
        return x, y
    
    def y_x(self, x):
        
        return self.Byx(x)
   
    def s_x(self, x):
        
        return self.Bsx(x)
    
    def xy_t(self, tnorm):
        
        t = np.asarray(tnorm*self.t[-1]).copy()
        x = np.full(np.shape(t), 0.)        
        y = np.full(np.shape(t), 0.)        
        
        for i in range(np.size(self.Bxt)):
            
            ii = np.logical_and( t >= self.t[i], t <= self.t[i+1] )

            x[ii] = self.Bxt[i](t[ii])
            y[ii] = self.Byt[i](t[ii])
            
        return x, y

    def theta(self, snorm):

        s = np.asarray(snorm*self.s[-1]).copy()
        xd = np.full(np.shape(s), 0.)        
        yd = np.full(np.shape(s), 0.)        

        for i in range(np.size(self.Bxt)):

            ii = np.logical_and( s >= self.s[i], s <= self.s[i+1] )

            t = self.Bts[i](s[ii])
            
            xd[ii] = self.Bxt[i](t, 1)
            yd[ii] = self.Byt[i](t, 1)
        
        return np.arctan2(yd, xd)

    def theta_t(self, tnorm):

        t = np.asarray(tnorm*self.t[-1]).copy()
        xd = np.full(np.shape(t), 0.)        
        yd = np.full(np.shape(t), 0.)        

        for i in range(np.size(self.Bxt)):

            ii = np.logical_and( t >= self.t[i], t <= self.t[i+1] )

            xd[ii] = self.Bxt[i](t[ii], 1)
            yd[ii] = self.Byt[i](t[ii], 1)
        
        return np.arctan2(yd, xd)

    def K(self, snorm):

        s = np.asarray(snorm*self.s[-1]).copy()
        xd = np.full(np.shape(s), 0.)        
        yd = np.full(np.shape(s), 0.)        
        xdd = np.full(np.shape(s), 0.)        
        ydd = np.full(np.shape(s), 0.)        

        for i in range(np.size(self.Bxt)):

            ii = np.logical_and( s >= self.s[i], s <= self.s[i+1] )

            t = self.Bts[i](s[ii])
            
            xd[ii] = self.Bxt[i](t, 1)
            yd[ii] = self.Byt[i](t, 1)
            xdd[ii] = self.Bxt[i](t, 2)
            ydd[ii] = self.Byt[i](t, 2)
        
        return (xd*ydd - yd*xdd)/((xd*xd + yd*yd)**1.5)

    def K_t(self, tnorm):

        t = np.asarray(tnorm*self.t[-1]).copy()
        xd = np.full(np.shape(t), 0.)        
        yd = np.full(np.shape(t), 0.)        
        xdd = np.full(np.shape(t), 0.)        
        ydd = np.full(np.shape(t), 0.)        

        for i in range(np.size(self.Bxt)):

            ii = np.logical_and( t >= self.t[i], t <= self.t[i+1] )

            xd[ii] = self.Bxt[i](t[ii], 1)
            yd[ii] = self.Byt[i](t[ii], 1)
            xdd[ii] = self.Bxt[i](t[ii], 2)
            ydd[ii] = self.Byt[i](t[ii], 2)
        
        return (xd*ydd - yd*xdd)/((xd*xd + yd*yd)**1.5)
        
#    def derivative(self, nu=1):
#        
#        b = copy.deepcopy(self)
#        
#        for i in range(np.size(self.bs)):
#            
#            dsdx = self.bs[i].derivative()
#            dyds = self.by[i].derivative()
#            
#            # chain rule
#            
#            # y = ys(s(x))* yx(x)
#            # dydx = dysdx(s(x)) * dsdx(x) * yx(x) + ys(s(x)) * dyxdx(x)
#            
#            b.by[i] = dyds
#            #b.bs = self.bs
#            b.bx[i] = dsdx
#            
#        return b

