# -*- coding: utf-8 -*-
"""
Created on Sat Mar 17 17:43:26 2018

@author: Alex
"""

import numpy as np
import csv
from scipy import interpolate as interp
from scipy.integrate import cumtrapz
import scipy.optimize as opt

import matplotlib.pyplot as plt

class CLEuler:

    def __init__(self, s=None, x=None, y=None, theta=None, K=None):
    
        if s is not None:
            self.set(s, x, y, theta, K)

    def read(self, csv_filename):
        
        with open(csv_filename) as f:
            
            csv.register_dialect('myDialect',
                                    quotechar='"', 
                                  delimiter=',',
                                  skipinitialspace=True, 
                                  lineterminator='\n', 
                                  strict=False)
            
            reader = csv.DictReader(f, dialect = 'myDialect')
            
            dsnorm = []
            x = []
            y = []
            theta = []
            K = []
            
            for row in reader:
                
                dsnorm.append(row['dsnorm'])
                x.append(row['x'])
                y.append(row['y'])
                theta.append(row['theta'])
                K.append(row['K'])
                
            dsnorm = np.array(dsnorm, dtype=np.float)
            x = np.array(x, dtype=np.float)
            y = np.array(y, dtype=np.float)
            theta = np.array(theta, dtype=np.float)*np.pi/180.
            K = np.array(K, dtype=np.float)
            
            print(dsnorm, x, y, theta, K)
            
            self.set(dsnorm, x, y, theta, K)

    def set(self, dsnorm, x, y, theta, K):
        
        npts = 1000
        korder = 3

        self.x0 = np.asarray(x).copy()
        self.y0 = np.asarray(y).copy()
        self.theta0 = np.asarray(theta).copy()
        self.K0 = np.asarray(K).copy()

        self.Bxs = []
        self.Bys = []
        self.s = [0]
        
        for i in range(np.size(self.x0)-1):

            xi = [x[i], x[i+1]]
            yi = [y[i], y[i+1]]

            dsmin = np.hypot(np.diff(xi), np.diff(yi))[0]
            dsmax = (np.abs(np.diff(xi)) + np.abs(np.diff(yi)))[0]
            
            ds = dsnorm[i+1]*(dsmin + dsmax)/2.
            
            si = [self.s[i], self.s[i] + ds]
            
            print('si =', si)

            thetai = [theta[i], theta[i+1]]
            Ki = [K[i], K[i+1]]
            
            sfull, Kfull = CLEuler.__solve(si, xi, yi, thetai, Ki)
            
            svec, xvec, yvec = CLEuler.__cubic_primative(x[i], y[i], theta[i], sfull, Kfull)

            # ensure the match of knot points is exact
            svec = np.hstack((self.s[i], svec[1:-1], sfull[-1]))
            xvec = np.hstack((xi[0], xvec[1:-1], xi[1]))
            yvec = np.hstack((yi[0], yvec[1:-1], yi[1]))
            
            dxds0 = np.cos(theta[i])
            dxds1 = np.cos(theta[i+1])
            d2xds20 = -K[i]*np.sin(theta[i])
            d2xds21 = -K[i+1]*np.sin(theta[i+1])

#            bcx = ([(1, dxds0), (2, d2xds20)], [(1, dxds1), (2, d2xds21)])
            bcx = ([(1, dxds0)], [(1, dxds1)])
            
            Bxs = interp.make_interp_spline(x=svec, y=xvec, k=korder, bc_type=bcx)

            dyds0 = np.sin(theta[i])
            dyds1 = np.sin(theta[i+1])
            d2yds20 =  K[i]*np.cos(theta[i])
            d2yds21 =  K[i+1]*np.cos(theta[i+1])
            
#            bcy = ([(1, dyds0), (2, d2yds20)], [(1, dyds1), (2, d2yds21)])
            bcy = ([(1, dyds0)], [(1, dyds1)])
           
            Bys = interp.make_interp_spline(x=svec, y=yvec, k=korder, bc_type=bcy)
            
            # mapping from s to x
            self.Bxs.append(Bxs)

            # mapping from s to y
            self.Bys.append(Bys)

            # s knot values
            self.s.append(sfull[-1])

        snorm = np.linspace(0., 1.0, npts)
        
        xs, ys = self.__call__(snorm)

        try:
            self.Byx = interp.PchipInterpolator(x=xs, y=ys)
            self.Bsx = interp.PchipInterpolator(x=xs, y=snorm)
        except:
            self.Byx = None
            self.Bsx = None

    # x0 - initial X
    # y0 - initial y
    # theta0 - initial atan2(dy,dx)
    # s - path length knot points
    # K - curvature knot points
    def __cubic_primative(x0, y0, theta0, s, K):
    
        npts = 1000
        
        # piecewise linear mapping from s to K
        
        s = np.asarray(s, dtype=np.float).copy()
        K = np.asarray(K, dtype=np.float).copy()
        
        Bks = interp.make_interp_spline(x=s, y=K, k=1, bc_type=None)
        
        svec = np.linspace(s[0], s[-1], npts)
        
        theta = theta0 + np.array([Bks.integrate(s[0], si) for si in svec])
    
        ds = (s[-1] - s[0])/(npts-1)
        
        x = x0 + np.hstack((0., cumtrapz(ds*np.cos(theta))))
        y = y0 + np.hstack((0., cumtrapz(ds*np.sin(theta))))
        
        return (svec, x, y)

    def __state0(z, s, K, x, y, theta):
        
        del_theta = theta[1] - theta[0]
        
        ds = s[1] - s[0]
        s0 = s[0]
        s1 = s0 + z[1]*z[0]
        s3 = s0 + z[1]*ds
        s2 = s1 + 0.5*(s3 - s1)

        K0 = K[0]
        K3 = K[1]
        K1 = z[2]
        K2 = (2.*del_theta - (K1 + K0)*(s1 - s0) - K1*(s2 - s1) - K3*(s3 - s2)) / (s3 - s1)
        
        sret = np.array([s0, s1, s2, s3])
        Kret = np.array([K0, K1, K2, K3])

        return (sret, Kret)

    def __f0(z, s, K, x, y, theta):

        c0 = 1.e7
        c1 = 0.0
        c2 = 0.0
        c3 = 1.
        
        sret, Kret = CLEuler.__state0(z, s, K, x, y, theta)
        KP0 = (Kret[1]- Kret[0])/(sret[1]- sret[0])
        KP1 = (Kret[2]- Kret[1])/(sret[2]- sret[1])
        KP2 = (Kret[3]- Kret[2])/(sret[3]- sret[2])
        
        _, xret, yret = CLEuler.__cubic_primative(x[0], y[0], theta[0], sret, Kret)

        return c0*((x[1] - xret[-1])**2. + (y[1] - yret[-1])**2) \
                + c1*(Kret[1]**2. + c1*Kret[2]**2.) \
                + c2*np.max([KP0**2., KP1**2.,KP2**2.]) \
                + c3*((sret[3]-sret[0]) - (s[1]-s[0]))**2.

    def __f0_ieqcons(z, s, K, x, y, theta):
        
        c0 = 1.e4

        sret, Kret = CLEuler.__state0(z, s, K, x, y, theta)

#        dtheta0 = np.abs(0.5*(Kret[1]+Kret[0])*(sret[1]-sret[0]))
#        dtheta1 = np.abs(0.5*(Kret[2]+Kret[1])*(sret[2]-sret[1]))
#        dtheta2 = np.abs(0.5*(Kret[3]+Kret[2])*(sret[3]-sret[2]))

        if Kret[0]*Kret[1] < 0:
            # from initial to zero crossing
            dtheta0a = np.abs(0.5*(Kret[0]**2)*(sret[1]-sret[0])/(Kret[1]-Kret[0]))

            # from zero crossing to final
            dtheta0b = np.abs(0.5*(Kret[1]**2)*(sret[1]-sret[0])/(Kret[1]-Kret[0]))
            
            dtheta0 = np.max([dtheta0a, dtheta0b])
        else:
            dtheta0 = np.abs(0.5*(Kret[1]+Kret[0])*(sret[1]-sret[0]))

        if Kret[1]*Kret[2] < 0:
            # from initial to zero crossing
            dtheta1a = np.abs(0.5*(Kret[1]**2)*(sret[2]-sret[1])/(Kret[2]-Kret[1]))

            # from zero crossing to final
            dtheta1b = np.abs(0.5*(Kret[2]**2)*(sret[2]-sret[1])/(Kret[2]-Kret[1]))

            dtheta1 = np.max([dtheta1a, dtheta1b])
        else:
            dtheta1 = np.abs(0.5*(Kret[2]+Kret[1])*(sret[2]-sret[1]))

        if Kret[2]*Kret[3] < 0:
            # from initial to zero crossing
            dtheta2a = np.abs(0.5*(Kret[2]**2)*(sret[3]-sret[2])/(Kret[3]-Kret[2]))

            # from zero crossing to final
            dtheta2b = np.abs(0.5*(Kret[3]**2)*(sret[3]-sret[2])/(Kret[3]-Kret[2]))

            dtheta2 = np.max([dtheta2a, dtheta2b])
        else:
            dtheta2 = np.abs(0.5*(Kret[3]+Kret[2])*(sret[3]-sret[2]))
        
        return c0 * np.array([np.pi - dtheta0,
                         np.pi - dtheta1,
                         np.pi - dtheta2
                         ])

    def __state1(z, s, K, x, y, theta):
        
        del_theta = theta[1] - theta[0]
        
        s0 = s[0]
        s1 = s0 + z[0]
        s2 = s1 + z[1]
        s3 = s2 + z[2]

        K0 = K[0]
        K3 = K[1]
        K1 = z[3]
        K2 = (2.*del_theta - (K1 + K0)*(s1 - s0) - K1*(s2 - s1) - K3*(s3 - s2)) / (s3 - s1)
        
        sret = np.array([s0, s1, s2, s3])
        Kret = np.array([K0, K1, K2, K3])

        return (sret, Kret)

    def __f1(z, s, K, x, y, theta):
        
        c0 = 10.0
        c1 = 0.05
        c2 = 0.05
        
        sret, Kret = CLEuler.__state1(z, s, K, x, y, theta)
        
        KP0 = (Kret[1] - Kret[0])/(sret[1] - sret[0])
        KP1 = (Kret[2] - Kret[1])/(sret[2] - sret[1])
        KP2 = (Kret[3] - Kret[2])/(sret[3] - sret[2])

        return c0*np.max(np.array([KP0**2., KP1**2., KP2**2.])) \
                + c1*Kret[1]**2. + c1*Kret[2]**2. \
                + c2*((sret[3]-sret[0]) / (s[1]-s[0]))**2.

    def __f1_eqcons(z, s, K, x, y, theta):
        
        c0 = 1.e10
        
        sret, Kret = CLEuler.__state1(z, s, K, x, y, theta)
        
        _, xret, yret = CLEuler.__cubic_primative(x[0], y[0], theta[0], sret, Kret)

        r = np.hypot(xret[-1] - x[1], yret[-1] - y[1])
        
        return np.array([c0*r])
    
    def __f1_ieqcons(z, s, K, x, y, theta):
        
        c0 = 1.e4
#        eps = 1.e-3
        
        sret, Kret = CLEuler.__state1(z, s, K, x, y, theta)
#        _, xret, yret = CLEuler.__cubic_primative(x[0], y[0], theta[0], sret, Kret)
        
#        r = np.hypot(xret[-1] - x[1], yret[-1] - y[1])

        if Kret[0]*Kret[1] < 0:
            # from initial to zero crossing
            dtheta0a = np.abs(0.5*(Kret[0]**2)*(sret[1]-sret[0])/(Kret[1]-Kret[0]))

            # from zero crossing to final
            dtheta0b = np.abs(0.5*(Kret[1]**2)*(sret[1]-sret[0])/(Kret[1]-Kret[0]))
            
            dtheta0 = np.max([dtheta0a, dtheta0b])
        else:
            dtheta0 = np.abs(0.5*(Kret[1]+Kret[0])*(sret[1]-sret[0]))

        if Kret[1]*Kret[2] < 0:
            # from initial to zero crossing
            dtheta1a = np.abs(0.5*(Kret[1]**2)*(sret[2]-sret[1])/(Kret[2]-Kret[1]))

            # from zero crossing to final
            dtheta1b = np.abs(0.5*(Kret[2]**2)*(sret[2]-sret[1])/(Kret[2]-Kret[1]))

            dtheta1 = np.max([dtheta1a, dtheta1b])
        else:
            dtheta1 = np.abs(0.5*(Kret[2]+Kret[1])*(sret[2]-sret[1]))

        if Kret[2]*Kret[3] < 0:
            # from initial to zero crossing
            dtheta2a = np.abs(0.5*(Kret[2]**2)*(sret[3]-sret[2])/(Kret[3]-Kret[2]))

            # from zero crossing to final
            dtheta2b = np.abs(0.5*(Kret[3]**2)*(sret[3]-sret[2])/(Kret[3]-Kret[2]))

            dtheta2 = np.max([dtheta2a, dtheta2b])
        else:
            dtheta2 = np.abs(0.5*(Kret[3]+Kret[2])*(sret[3]-sret[2]))
        
        return c0 * np.array([np.pi - dtheta0,
                         np.pi - dtheta1,
                         np.pi - dtheta2
                         ])

#        return c0 * np.array([ (eps - r)/eps,
#                         np.pi - dtheta0,
#                         np.pi - dtheta1,
#                         np.pi - dtheta2,
#                         np.pi - dtheta0a,
#                         np.pi - dtheta0b,
#                         np.pi - dtheta1a,
#                         np.pi - dtheta1b,
#                         np.pi - dtheta2a,
#                         np.pi - dtheta2b
#                         ])
#        

    def __solve(s, x, y, theta, K):

        # step 0 -- find Euler spiral that intersects endpoints
        f0 = lambda z: CLEuler.__f0(z, s, K, x, y, theta)
        f0_ieqcons = lambda z: CLEuler.__f0_ieqcons(z, s, K, x, y, theta)
        
        z0 = np.array([0.33*(s[1]-s[0]), 1.0, 0.])

        smin = 0.1*(s[1]-s[0])
        smax = 2.*(s[1]-s[0])

        bounds = [(smin, smax), (0.5, 2.0), (-np.inf, np.inf)]
        
        zout, fx, its, imode, smode = opt.fmin_slsqp(func = f0, x0=z0, 
                  bounds = bounds, acc = 1e-4, epsilon = 1e-4, f_ieqcons = f0_ieqcons,
                  disp = 2, iter = 100, full_output = True)
        
        sfull, Kfull = CLEuler.__state0(zout, s, K, x, y, theta)

        print('f0 =', f0(zout))
        print('__f0 =', CLEuler.__f0(zout, s, K, x, y, theta))

        print('f0_ieqcons =', f0_ieqcons(zout))

        print('sfull =', sfull)
        print('Kfull =', Kfull)

        svec, xret, yret = CLEuler.__cubic_primative(x[0], y[0], theta[0], sfull, Kfull)
    
        fig1 = plt.figure()
        ax1 = fig1.gca()
        ax1.plot(xret, yret, 'b-')
        ax1.plot(x, y, 'r.')
    
        plt.ylabel('y')
        plt.xlabel('x')
        plt.axis('equal')
        plt.grid()

        fig4 = plt.figure()
        ax4 = fig4.gca()
        ax4.plot(sfull, Kfull, 'b-')
        ax4.plot(sfull, Kfull, 'r.')
        
        plt.ylabel('K')
        plt.xlabel('s')
        plt.grid()

        plt.draw()
        plt.show()

#        assert(fx < 1e2)
        assert(np.all(f0_ieqcons(zout) >= 0.))
        assert(imode == 0)
        print(smode)

        ## step 1 -- minimize jerk
        f1 = lambda z: CLEuler.__f1(z, s, K, x, y, theta)
        f1_eqcons = lambda z: CLEuler.__f1_eqcons(z, s, K, x, y, theta)
#        f1_neqcons = lambda z: -CLEuler.__f1_eqcons(z, s, K, x, y, theta)
        f1_ieqcons = lambda z: CLEuler.__f1_ieqcons(z, s, K, x, y, theta)
#        f1_cons = lambda z: np.hstack((f1_ieqcons(z), f1_eqcons(z), f1_neqcons(z)))

#        z1 =  np.array([0.03608504,   0.16101442,   0.16101442, -15.65336873])
        z1 = np.array([sfull[1] - sfull[0],
                       sfull[2] - sfull[1],
                       sfull[3] - sfull[2],
                       Kfull[1]])

        print('z1 = ', z1)
        print('f1_ieqcons(z1) =', f1_ieqcons(z1))
    
        smin = 0.01*(s[1]-s[0])
        smax = 2.*(s[1]-s[0])
        bounds = [(smin , smax), (smin , smax), (smin , smax), (-np.inf, np.inf)]
#        lb = np.array([smin, smin, smin, -20/(s[1]-s[0])])
#        ub = np.array([smax, smax, smax, 20/(s[1]-s[0])])

        zout, fx, its, imode, smode = opt.fmin_slsqp(func = f1, x0 = z1, 
                  bounds = bounds, f_eqcons = f1_eqcons, 
                  f_ieqcons = f1_ieqcons, acc = 1e-6,
                  disp = 2, iter = 40, full_output = True)

#        zout, fx, = pso(f1, lb, ub, 
#                  f_ieqcons = f1_ieqcons, swarmsize  = 100,
#                  omega = 0.1, phip = 0.1, phig = 0.1,
#                  maxiter = 20, debug  = True)
        
        print('f1 =', f1(zout))
        print('f1_ieqcons =', f1_ieqcons(zout))

        assert(np.all(f1_ieqcons(zout) >= 0.))
#        assert(imode == 0)

        sfull, Kfull = CLEuler.__state1(zout, s, K, x, y, theta)

#        theta1 = theta[0] + 0.5*np.sum((Kfull[0:-1] + Kfull[1:])*np.diff(sfull))

        print('sfull =', sfull)
        print('Kfull =', Kfull)


        svec, xret, yret = CLEuler.__cubic_primative(x[0], y[0], theta[0], sfull, Kfull)
    
        fig1 = plt.figure()
        ax1 = fig1.gca()
        ax1.plot(xret, yret, 'b-')
        ax1.plot(x, y, 'r.')
    
        plt.ylabel('y')
        plt.xlabel('x')
        plt.axis('equal')
        plt.grid()
    
#        fig2 = plt.figure()
#        ax2 = fig2.gca()
#        ax2.plot(svec, xret, 'b-')
#        ax2.plot(s, x, 'r.')
#    
#        plt.ylabel('x')
#        plt.xlabel('s')
#        plt.grid()
#    
#        fig3 = plt.figure()
#        ax3 = fig3.gca()
#        ax3.plot(svec, yret, 'b-')
#        ax3.plot(s, y, 'r.')
#        
#        plt.ylabel('y')
#        plt.xlabel('s')
#        plt.grid()
#    
        fig4 = plt.figure()
        ax4 = fig4.gca()
        ax4.plot(sfull, Kfull, 'b-')
        ax4.plot(sfull, Kfull, 'r.')
        
        plt.ylabel('K')
        plt.xlabel('s')
        plt.grid()
    
        plt.draw()
        plt.show()


        return (sfull, Kfull)

    def __call__(self, snorm):

        s = np.asarray(snorm*self.s[-1]).copy()
        x = np.full(np.shape(s), 0.)        
        y = np.full(np.shape(s), 0.)        
        
        for i in range(np.size(self.Bxs)):
            
            ii = np.logical_and( s >= self.s[i], s <= self.s[i+1] )

            x[ii] = self.Bxs[i](s[ii])
            y[ii] = self.Bys[i](s[ii])
            
        return x, y
    
    def y_x(self, x):
        
        return self.Byx(x)
   
    def s_x(self, x):
        
        return self.Bsx(x)
    
    def theta(self, snorm):

        s = np.asarray(snorm*self.s[-1]).copy()
        xd = np.full(np.shape(s), 0.)        
        yd = np.full(np.shape(s), 0.)        

        for i in range(np.size(self.Bxs)):

            ii = np.logical_and( s >= self.s[i], s <= self.s[i+1] )

            xd[ii] = self.Bxs[i](s[ii], 1)
            yd[ii] = self.Bys[i](s[ii], 1)
        
        return np.arctan2(yd, xd)

    def K(self, snorm):

        s = np.asarray(snorm*self.s[-1]).copy()
        xd = np.full(np.shape(s), 0.)        
        yd = np.full(np.shape(s), 0.)        
        xdd = np.full(np.shape(s), 0.)        
        ydd = np.full(np.shape(s), 0.)        

        for i in range(np.size(self.Bxs)):

            ii = np.logical_and( s >= self.s[i], s <= self.s[i+1] )

            xd[ii] = self.Bxs[i](s[ii], 1)
            yd[ii] = self.Bys[i](s[ii], 1)
            xdd[ii] = self.Bxs[i](s[ii], 2)
            ydd[ii] = self.Bys[i](s[ii], 2)
        
        return (xd*ydd - yd*xdd)/((xd*xd + yd*yd)**1.5)

