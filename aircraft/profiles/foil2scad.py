#!/usr/bin/env python

#################################################################################
# This file is part of airfoil-utils.
#
# airfoil-utils is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# airfoil-utils is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with airfoil-utils.  If not, see <http://www.gnu.org/licenses/>.
#
#################################################################################
#
# Script to convert airfoil coordinate .dat files into SVG format, with minor
# additional attributes to make them easy to view in Inkscape.
#
# Currently, this script has only been tested on files from:
#
# http://m-selig.ae.illinois.edu/ads/coord_database.html
#
# Author: John Casey (jdcasey@commonjava.org)
#
#################################################################################

import os
import argparse
from urllib.parse import urlparse
import solid
import numpy as np
from foil import Foil
from datetime import datetime

# work-around to fix OpenSCAD's known issue of culling polygon resolution
# see https://github.com/openscad/openscad/issues/999
OSCAD_RESCALE_FUDGE = 1000.

# Return a 1 mm chord length OpenPySCAD polygon object for the specified 
# airfoil.  See:
# https://github.com/taxpon/openpyscad
# https://pypi.python.org/pypi/OpenPySCAD
def Polygon(*, dat_filename=None, uiuc_filename=None, url=None):

    return toScad(Foil(dat_filename=dat_filename, uiuc_filename=uiuc_filename, url=url, interpolate=300))

# write a SCAD file of the specified airfail
def Process(*, dat_filename=None, uiuc_filename=None, url=None, output_filename=None):
    
    p, airfoil_name = Polygon(dat_filename = dat_filename, uiuc_filename=uiuc_filename, url=url)

    if dat_filename is not None:
        airfoil_name = os.path.splitext(os.path.basename(dat_filename))[0]
    elif uiuc_filename is not None:
        airfoil_name = os.path.splitext(os.path.basename(uiuc_filename))[0]
    elif url is not None:
        u = urlparse(url)
        airfoil_name = os.path.splitext(os.path.basename(u.path))[0]

    if output_filename is None:
        output_filename = "%s.scad" % airfoil_name

    solid.scad_render_to_file(p, output_filename)
    
    __add_header(output_filename, airfoil_name)
    
# return a SCAD object from a list or numpy array of points
def toScad(points):
    
    agSurf = (OSCAD_RESCALE_FUDGE*np.asarray(points)).tolist()
    
    # Polygon appears to use the list print directly, so if we give it a numpy 
    # array the output comes out without commas between points
    return solid.scale(1./OSCAD_RESCALE_FUDGE)(solid.polygon(agSurf))

# add a header row with the source information
def __add_header(filename, airfoil_name):

    header = '// ' + airfoil_name + ' written by foil2scad.py ' + datetime.isoformat(datetime.utcnow()) + '\n'
    
    with open(filename, 'r') as f:
        lines = f.readlines()

    lines.insert(0,header)

    # output with header added
    with open(filename, 'w') as f:
        for line in lines:
            f.write(line)    

if __name__ == "__main__":

    parser = argparse.ArgumentParser()
    parser.add_argument("DAT", help="DAT file or DAT name (if using UIUC download)")
    parser.add_argument("SCAD", nargs="?", help="OpenSCAD output file (defaults to <DAT>.scad)")
    parser.add_argument("-u", "--url", help="Download the specified DAT name from the UIUC Airfoil Database", action="store_true")
    
    args = parser.parse_args()

    dat_filename=args.DAT
    output_filename=args.SCAD or None

    if args.url:
        Process(uiuc_filename=dat_filename, output_filename=output_filename)
    else:
        Process(dat_filename=args.DAT, output_filename=output_filename)
