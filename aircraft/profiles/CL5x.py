# -*- coding: utf-8 -*-
"""
Created on Sat Mar 17 17:28:48 2018

@author: Alex
"""

import numpy as np
import csv
from scipy import interpolate as interp
import copy

class CL5x:

    def __init__(self, x=None, y=None, theta=None, K=None):
    
        if x is not None:
            self.set(x, y, theta, K)

    def read(self, csv_filename):
        
        with open(csv_filename) as f:
            
            csv.register_dialect('myDialect',
                                  quotechar='"', 
                                  delimiter=',',
                                  skipinitialspace=True, 
                                  lineterminator='\n', 
                                  strict=False)
            
            reader = csv.DictReader(f, dialect = 'myDialect')
            
            x = []
            y = []
            theta = []
            K = []
            
            for row in reader:
                
                x.append(row['x'])
                y.append(row['y'])
                theta.append(row['theta'])
                K.append(row['K'])
                
            x = np.array(x, dtype=np.float)
            y = np.array(y, dtype=np.float)
            theta = np.array(theta, dtype=np.float)*np.pi/180.
            K = np.array(K, dtype=np.float)
            
            print(x, y, theta, K)
            
            self.set(x, y, theta, K)

    def set(self, xvec, yvec, thetavec, kvec):
    
        korder = 5

        xvec = np.asarray(xvec).copy()
        yvec = np.asarray(yvec).copy()
        thetavec = np.asarray(thetavec).copy()
        kvec = np.asarray(kvec).copy()

        self.x0 = xvec
        self.y0 = yvec
        self.theta0 = thetavec
        self.K0 = kvec

        self.Byx = []
        
        for i in range(np.size(xvec)-1):
        
            xi = np.array([xvec[i], xvec[i+1]])
            yi = np.array([yvec[i], yvec[i+1]])
            
            bcy = ([(1, np.tan(thetavec[i])), (2, kvec[i])], [(1, np.tan(thetavec[i+1])), (2, kvec[i+1])])
            self.Byx.append(interp.make_interp_spline(x=xi, y=yi, k=korder, bc_type=bcy))
            
    def __call__(self, x):

        x = np.asarray(x).copy()
        y = np.full(np.shape(x), 0.)        

        for i in range(np.size(self.by)):
            
            ii = np.logical_and( x >= self.x0[i], x <= self.x0[i+1] )
            y[ii] = self.Byx[i](x[ii])
            
        return y
    
    def derivative(self, nu=1):
        
        b = copy.deepcopy(self)
        
        for i in range(np.size(self.by)):
            
            b.Byx[i] = self.Byx[i].derivative(nu)
            
        return b

    def theta(self, x):

        x = np.asarray(x).copy()
        yd = np.full(np.shape(x), 0.)        

        for i in range(np.size(self.Byx)):

            ii = np.logical_and( x >= self.x0[i], x <= self.x0[i+1] )
            
            yd[ii] = self.Byx[i](x[ii], 1)
        
        return np.arctan(yd)
