# -*- coding: utf-8 -*-
"""
Created on Sat Mar 17 20:10:14 2018

@author: Alex
"""

import numpy as np
from scipy import interpolate as interp

from geomdl import NURBS
from geomdl import utilities
#from geomdl.visualization import VisMPL

class CLNURBS:

    def __init__(self, xw=None, yw=None, w=None):

        self.curve = NURBS.Curve()
        self.curve.delta = 0.001
        self.curve.degree = 4
        
        self.xmin = 0.
        self.xmax = 0.
        
        if xw is not None:
            self.set(xw, yw, w)

    def read(self, filename):
        
        self.curve.read_ctrlpts_from_txt(filename)
        self.reinterp()

    def set(self, xw, yw, w):

        xw = np.asarray(xw).copy()
        yw = np.asarray(yw).copy()
        w = np.asarray(w).copy()
        
        n = np.size(xw)
        
        xw = np.reshape(xw, (n,1))
        yw = np.reshape(yw, (n,1))
        w = np.reshape(w, (n,1))
        
        self.curve.ctrlpts = np.hstack((xw, yw, w)).tolist()
        self.reinterp()
  
    def reinterp(self):
        
        self.curve.knotvector = utilities.generate_knot_vector(self.curve.degree, len(self.curve.ctrlpts))
        self.curve.evaluate()

#        vis_config = VisMPL.VisConfig(figure_size=[8, 8])
#        vis_comp = VisMPL.VisCurve2D(config=vis_config)
#        self.curve.vis = vis_comp
#        self.curve.render()
        
#        uvec = np.linspace()
        
        c = np.array(self.curve.curvepts)
        
#        u = np.arange(0., 1., self.curve.delta)
        
        x = c[:,0]
        y = c[:,1]
        
        x[0] = self.curve.ctrlpts[0][0]
        x[-1] = self.curve.ctrlpts[-1][0]
        y[0] = self.curve.ctrlpts[0][1]
        y[-1] = self.curve.ctrlpts[-1][1]

        self.xmin = np.min(x)
        self.xmax = np.max(x)
        
        assert(np.all(np.diff(x) > 0.))
        
        self.Byx = interp.CubicSpline(x=x, y=y)
        

    def __call__(self, x):

        return self.Byx(np.asarray(x))
    
#    def derivative(self, nu=1):
#        
#        b = copy.deepcopy(self)
#        
#        for i in range(np.size(self.by)):
#            
#            b.Byx[i] = self.Byx[i].derivative(nu)
#            
#        return b
#
#    def theta(self, x):
#
#        x = np.asarray(x).copy()
#        yd = np.full(np.shape(x), 0.)        
#
#        for i in range(np.size(self.Byx)):
#
#            ii = np.logical_and( x >= self.x0[i], x <= self.x0[i+1] )
#            
#            yd[ii] = self.Byx[i](x[ii], 1)
#        
#        return np.arctan(yd)
