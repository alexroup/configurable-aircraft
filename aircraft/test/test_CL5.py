# -*- coding: utf-8 -*-
"""
Created on Sat Mar 17 17:52:07 2018

@author: Alex
"""

import numpy as np
import matplotlib.pyplot as plt

from aircraft.profiles.CL5 import CL5

def test_CL500():
    
    npts = 1000
    
    dels = np.array([1.1, 0.45, 1.7])
#    sk = np.array([0.,1.1, 1.55, 3.25])
    xk = [0.,0.25, 0.7, 2.]
    yk = [0.0, 1., 1., 0.1]
    
    thetak = [np.pi/2, 0.0, 0., -np.pi/8]
    Kk = [0., 0.,  0., 0.]

    s = np.linspace(0., 1., npts)
    sfac = np.linspace(0.5, 1.2, 20)

    fig1 = plt.figure()
    ax1 = fig1.gca()

    fig2 = plt.figure()
    ax2 = fig2.gca()

    fig3 = plt.figure()
    ax3 = fig3.gca()

    for sf in sfac:
        try:
            
            sk = np.array([0.,dels[0]*sf, dels[0]*sf + dels[1], dels[0]*sf + dels[1] + dels[2]*sf])
            
            B = CL5(sk, xk, yk, thetak, Kk)
            x, y = B(s)
            ax1.plot(x, y, '-')
            ax2.plot(x, B.K(s), '-')
            ax3.plot(x, B.theta(s), '-')
        except:
            pass

#    B = CL5(sk, xk, yk, thetak, Kk)
#    x, y = B(s)
    
#    print('x =', x)
#    print('y =', y)
    

#    ax1.plot(x, y, '-')
#    ax2.plot(x, B.K(s), '-')
#    ax3.plot(x, B.theta(s), '-')

    ax1.plot(xk, yk, '.')
    ax2.plot(xk, Kk, '.')
    ax3.plot(xk, thetak, '.')

    plt.figure(fig1.number)
    plt.ylabel('y')
    plt.xlabel('x')
    plt.axis('equal')
    plt.grid()

    plt.figure(fig2.number)
    plt.ylabel('K')
    plt.xlabel('x')
    plt.ylim([-10,10])
    plt.grid()

    plt.figure(fig3.number)
    plt.ylabel('theta')
    plt.xlabel('x')
    plt.ylim([-4,4])
    plt.grid()

    plt.draw()
    plt.show()

    return

    xs = np.linspace(0., 2., npts)
    ys = B.y_x(xs)

    fig = plt.figure()
    ax = fig.gca()
    ax.plot(xs, ys, '-', xk, yk, '.')
    plt.ylabel('y')
    plt.xlabel('x')
    ax.axis('equal')
    plt.grid()
    plt.draw()
    plt.show()

def test_CL501():
    
    npts = 1000
    
    B = CL5()
    
    B.read('../examples/profiles/fuse_a0.csv')

    s = np.linspace(0., 1., npts)
    x, y = B(s)

    fig1 = plt.figure()
    ax1 = fig1.gca()
    ax1.plot(x, y, '-')
    ax1.plot(B.x0, B.y0, 'r.')

    plt.ylabel('y')
    plt.xlabel('x')
    plt.axis('equal')
    plt.grid()

    fig2 = plt.figure()
    ax2 = fig2.gca()
    ax2.plot(x, B.K(s), '-')
    ax2.plot(B.x0, B.K0, 'r.')

    plt.ylabel('K')
    plt.xlabel('x')
    plt.ylim([-10,10])
    plt.grid()

    fig3 = plt.figure()
    ax3 = fig3.gca()
    ax3.plot(x, B.theta(s), '-')
    ax3.plot(B.x0, B.theta0, 'r.')
    
    plt.ylabel('theta')
    plt.xlabel('x')
    plt.ylim([-4,4])
    plt.grid()

    plt.draw()
    plt.show()
