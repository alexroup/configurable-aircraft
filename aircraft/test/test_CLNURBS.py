# -*- coding: utf-8 -*-
"""
Created on Sun Mar 18 13:31:51 2018

@author: Alex
"""

import numpy as np
import matplotlib.pyplot as plt

from CLNURBS import CLNURBS

def test_CLNURBS():
    
    npts = 10000
    
    C = CLNURBS()
    
    C.read('fuse_a0.cptw')
    
    cpt = np.array(C.curve.ctrlpts)
    
    w0 = np.array(C.curve.weights)

    ii = w0>0.

    print('cpt =', cpt)
    
    x0 = cpt[ii,0]
    y0 = cpt[ii,1]
    
    print('x0 =', x0)
    print('y0 =', y0)
    
    x = np.linspace(np.min(x0), np.max(x0), npts)
    y = C(x)

    fig1 = plt.figure()
    ax1 = fig1.gca()
    ax1.plot(x, y, '-')
    ax1.plot(x0, y0, 'r.')

    plt.ylabel('y')
    plt.xlabel('x')
    plt.axis('equal')
    plt.grid()
