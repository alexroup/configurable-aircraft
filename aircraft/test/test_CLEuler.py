# -*- coding: utf-8 -*-
"""
Created on Sat Mar 17 17:48:27 2018

@author: Alex
"""

import numpy as np
import matplotlib.pyplot as plt

from CLEuler import CLEuler


def test_CLEuler00():
    
#    npts = 1000
#    
#    B = CLEuler()
#    
#    B.read('fuse_a0.csv')
#    
    
    s = [0., 2.5]
    x = [0., 1.8]
    y = [0., 0.]
    theta = [np.pi/2, -0.3]
    K = [-1, -1]
    
    sfull, Kfull = CLEuler._CLEuler__solve(s, x, y, theta, K)
    
#    print('sfull =', sfull)
#    print('Kfull =', Kfull)
#    print('theta1 =', theta1)
    
    svec, xret, yret = CLEuler._CLEuler__cubic_primative(x[0], y[0], theta[0], sfull, Kfull)

    print('xret[-1] =', xret[-1])
    print('yret[-1] =', yret[-1])

    print('xret[-1]-x[1] =', xret[-1]-x[1])
    print('yret[-1]-y[1] =', yret[-1]-y[1])


    fig1 = plt.figure()
    ax1 = fig1.gca()
    ax1.plot(xret, yret, 'b-')
    ax1.plot(x, y, 'r.')

    plt.ylabel('y')
    plt.xlabel('x')
    plt.axis('equal')
    plt.grid()

    fig2 = plt.figure()
    ax2 = fig2.gca()
    ax2.plot(svec, xret, 'b-')
    ax2.plot(s, x, 'r.')

    plt.ylabel('x')
    plt.xlabel('s')
    plt.grid()

    fig3 = plt.figure()
    ax3 = fig3.gca()
    ax3.plot(svec, yret, 'b-')
    ax3.plot(s, y, 'r.')
    
    plt.ylabel('y')
    plt.xlabel('s')
    plt.grid()

    plt.draw()
    plt.show()

    fig4 = plt.figure()
    ax4 = fig4.gca()
    ax4.plot(sfull, Kfull, 'b-')
    ax4.plot(sfull, Kfull, 'r.')
    
    plt.ylabel('K')
    plt.xlabel('s')
    plt.grid()

    plt.draw()
    plt.show()


def test_CLEuler01():
    
    npts = 10000
    
    B = CLEuler()
    
    B.read('fuse_a0.csv')

    s = np.linspace(0., 1., npts)
    x, y = B(s)

    fig1 = plt.figure()
    ax1 = fig1.gca()
    ax1.plot(x, y, '-')
    ax1.plot(B.x0, B.y0, 'r.')

    plt.ylabel('y')
    plt.xlabel('x')
    plt.axis('equal')
    plt.grid()

    fig2 = plt.figure()
    ax2 = fig2.gca()
    ax2.plot(s, B.K(s), '-')
    ax2.plot(B.x0, B.K0, 'r.')

    plt.ylabel('K')
    plt.xlabel('s')
#    plt.ylim([-10,10])
    plt.grid()

    fig3 = plt.figure()
    ax3 = fig3.gca()
    ax3.plot(s, B.theta(s), '-')
    ax3.plot(B.x0, B.theta0, 'r.')
    
    plt.ylabel('theta')
    plt.xlabel('s')
#    plt.ylim([-4,4])
    plt.grid()

    plt.draw()
    plt.show()
