#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Jun  2 19:42:10 2018

@author: aroup
"""

import fuselage.py


def test_fuselage():

#    Sfuse = bodyOML('fuselage/GA_example/fuse3.json', debug=True)

    Sfuse = bodyOML('fuselage/GA_example2/fuse.json', debug=True)
    Scanopy = bodyOML('fuselage/GA_example2/canopy.json', debug=True)
    
    S = Sfuse + solid.translate(v=[90, 0, 20]) (
                Scanopy
            )
    
#    S = Sfuse
    
    solid.scad_render_to_file(S, 'test_fuse.scad', file_header='$fn = 50;')
    

def make_test():

    a0 = [100, 200]
    b0 = 100
    n0 = [3,2.5]
    x0 = 0
    y0 = 10
    z0 = 20
    
    a1 = [100, 200]
    b1 = 100
    n1 = [3,2.5]
    x1 = 10
    y1 = 25
    z1 = 35

    a2 = [100, 200]
    b2 = 100
    n2 = [3,2.5]
    x2 = 20
    y2 = 30
    z2 = 40

    s = fuse_section2(a0, b0, n0, x0, y0, z0, a1, b1, n1, x1, y1, z1, a2, b2, n2, x2, y2, z2)
    
    solid.scad_render_to_file(s, 'test_section.scad', file_header='$fn = 50;')
