# -*- coding: utf-8 -*-
"""
Created on Sat Mar 17 17:32:37 2018

@author: Alex
"""
import numpy as np
import matplotlib.pyplot as plt

from CL5x import CL5x

plt.rcParams["figure.figsize"] = (15,10)

def test_CL5x():
    
    npts = 1000
    
    B = CL5x()
    
    B.read('fuse_xa0.csv')

    x = np.linspace(0., 1., npts)
    y = B(x)

    fig1 = plt.figure()
    ax1 = fig1.gca()
    ax1.plot(x, y, '-')
    ax1.plot(B.x0, B.y0, 'r.')

    plt.ylabel('y')
    plt.xlabel('x')
    plt.axis('equal')
    plt.grid()

#    fig2 = plt.figure()
#    ax2 = fig2.gca()
#    ax2.plot(x, B.K(x), '-')
#    ax2.plot(B.x0, B.K0, 'r.')
#
#    plt.ylabel('K')
#    plt.xlabel('x')
#    plt.ylim([-10,10])
#    plt.grid()
#
#    fig3 = plt.figure()
#    ax3 = fig3.gca()
#    ax3.plot(x, B.theta(x), '-')
#    ax3.plot(B.x0, B.theta0, 'r.')
#    
#    plt.ylabel('theta')
#    plt.xlabel('x')
#    plt.ylim([-4,4])
#    plt.grid()

    plt.draw()
    plt.show()

