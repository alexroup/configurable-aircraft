# -*- coding: utf-8 -*-
"""
Created on Sun Feb 11 21:28:14 2018

@author: Alex
"""

import foil
import numpy as np
import matplotlib.pyplot as plt

plt.rcParams["figure.figsize"] = (30,10)

#plt.rc('font',**{'family':'sans-serif','sans-serif':['Helvetica']})
#plt.rc('text', usetex=True)

points = foil.Foil(uiuc_filename='clarky', interpolate=False)

# this isn't actually the thickness, but it's close enough for what we need
thickness = np.max(points[:,1]) - np.min(points[:,1])

print('thickness:', thickness)

# calculate the linear length at each node

s = np.hstack(([0.], np.cumsum(np.hypot(np.diff(points[:,0]), np.diff(points[:,1])))))

dxds_0 = (points[1,0] - points[0,0])/(s[1]-s[0])
dxds_trap = (points[2:,0] - points[:-2,0])/(s[2:]-s[:-2])
dxds_1 = (points[-1,0] - points[-2,0])/(s[-1]-s[-2])

dyds_0 = (points[1,1] - points[0,1])/(s[1]-s[0])
dyds_trap = (points[2:,1] - points[:-2,1])/(s[2:]-s[:-2])
dyds_1 = (points[-1,1] - points[-2,1])/(s[-1]-s[-2])


dxds=np.hstack(([dxds_0], dxds_trap, [dxds_1]))
dyds=np.hstack(([dyds_0], dyds_trap, [dyds_1]))

ngrad = np.hypot(dxds, dyds)

dxds=dxds/ngrad
dyds=dyds/ngrad

print('dxds:', dxds)
print('dyds:', dyds)
print('norm:', np.hypot(dxds, dyds))

isupper = dxds < 0.
islower = np.logical_not(isupper)

plt.figure()
plt.plot(s, dxds)
plt.ylabel('dxds')
plt.xlabel('s')
plt.grid()
plt.show()

plt.figure()
plt.plot(s, dyds)
plt.ylabel('dyds')
plt.xlabel('s')
plt.grid()
plt.show()

print('unit vectors into the surface')
normal = np.transpose(np.vstack((-dyds, dxds)))
psi = np.arctan2(normal[:,0], normal[:,1])

#print('normal: ', normal)

rad = abs(points[:, 1])/abs(normal[:,1])

#print('rad: ', rad)

normdx = rad*normal[:,0]
normdy = rad*normal[:,1]

#print('normdx: ', normdx)
#print('normdy: ', normdy)

plt.figure()
plt.plot(points[isupper,0], points[isupper,1], 'r.-')
plt.plot(points[islower,0], points[islower,1], 'b.-')
plt.plot(points[:, 0] + np.vstack((0*normdx, normdx)), points[:,1] + np.vstack((0*normdy,normdy)), '.-' )
plt.grid()
plt.axis('equal')

#plt.xlim([0.85,1.01])
#
#plt.savefig('foil_zoom.png', dpi=600, facecolor='w', edgecolor='w',
#        orientation='landscape',
#        transparent=False, bbox_inches=None, pad_inches=0.1,
#        frameon=None)
#
plt.xlim([-0.05,1.05])

#plt.savefig('foil.png', dpi=600, facecolor='w', edgecolor='w',
#        orientation='landscape',
#        transparent=False, bbox_inches=None, pad_inches=0.1,
#        frameon=None)

plt.show()

plt.figure()
plt.plot(np.unwrap(psi), points[:,0])
plt.xlabel('psi')
plt.ylabel('x')
plt.grid()
plt.show()

plt.figure()
plt.plot(np.unwrap(psi), points[:,1])
plt.xlabel('psi')
plt.ylabel('y')
plt.grid()
plt.show()

print('phi to reference x value (sorta Joukowski)')

r = 1-thickness

theta = 0*points[:,0]
theta[isupper] = np.arccos(2*points[isupper,0] - 1)
theta[islower] = -np.arccos(2*points[islower,0] - 1)
theta = np.unwrap(theta)

xref = r/2*np.cos(theta) + 1 - r/2
dx = points[:,0] - xref
dy = points[:,1]
rad = np.hypot(dx,dy)
phi = np.arctan2(dx,dy)

plt.figure()
plt.plot(xref, points[:,0])
plt.ylabel('x')
plt.xlabel('xref')
plt.grid()
plt.show()

plt.figure()
plt.plot(theta, xref)
plt.xlabel('theta')
plt.ylabel('xref')
plt.grid()
plt.show()

plt.figure()
plt.plot(np.unwrap(theta), points[:,0])
plt.ylabel('x')
plt.xlabel('theta')
plt.grid()
plt.show()

plt.figure()
plt.plot(np.unwrap(theta), points[:,1])
plt.ylabel('y')
plt.xlabel('theta')
plt.grid()
plt.show()

plt.figure()
plt.plot(np.unwrap(theta), dx)
plt.ylabel('dx')
plt.xlabel('theta')
plt.grid()
plt.show()

plt.figure()
plt.plot(np.unwrap(theta), dy)
plt.ylabel('dy')
plt.xlabel('theta')
plt.grid()
plt.show()


plt.figure()
plt.plot(np.unwrap(theta), rad)
plt.ylabel('rad')
plt.xlabel('theta')
plt.grid()
plt.show()

plt.figure()
plt.plot(np.unwrap(theta), np.unwrap(phi))
plt.ylabel('phi')
plt.xlabel('theta')
plt.grid()
plt.show()


print('theta from x value, project back to chord line along theta to get radius')

dy = points[:,1]
dx = -dy*np.tan(theta)
rad = np.hypot(dx, dy)

plt.figure()
plt.plot(theta, dx)
plt.ylabel('dx')
plt.xlabel('theta')
plt.grid()
plt.show()

plt.figure()
plt.plot(theta, dy)
plt.ylabel('dy')
plt.xlabel('theta')
plt.grid()
plt.show()

plt.figure()
plt.plot(theta, rad)
plt.ylabel('rad')
plt.xlabel('theta')
plt.grid()
plt.show()


print('theta from x value, project back to chord line along normal')
print('generate new radius of offset circle from chord line intersection')

dy = points[:,1]
dx = dy*normal[:,0]
rad = np.hypot(dx, dy)

plt.figure()
plt.plot(theta[isupper], dx[isupper], 'r-', theta[islower], dx[islower], 'b-')
plt.ylabel('dx')
plt.xlabel('theta')
plt.grid()
plt.show()

plt.figure()
plt.plot(theta[isupper], dy[isupper], 'r-', theta[islower], dy[islower], 'b-')
plt.ylabel('dy')
plt.xlabel('theta')
plt.grid()
plt.show()

plt.figure()
plt.plot(theta[isupper], rad[isupper], 'r-', theta[islower], rad[islower], 'b-')
plt.ylabel('rad')
plt.xlabel('theta')
plt.grid()
plt.show()


print("airfoil space complex representation")
zeta = points[:,0] + 1j*points[:,1]

#zp = (zeta + np.sqrt(zeta**2 - 4))/2
#zm = (zeta - np.sqrt(zeta**2 - 4))/2
#
#x = np.linspace(0, 10, num=11, endpoint=True)
#y = np.cos(-x**2/9.0)
#f = interp1d(x, y)
#f2 = interp1d(x, y, kind='cubic')
#
#xnew = np.linspace(0, 10, num=41, endpoint=True)
#
#plt.plot(x, y, 'o', xnew, f(xnew), '-', xnew, f2(xnew), '--')
#plt.legend(['data', 'linear', 'cubic'], loc='best')
#plt.show()

plt.figure()
plt.plot(s, points[:,0], s, points[:,1])
plt.grid()
plt.show()

#plt.figure()
#plt.plot(np.real(zp), np.imag(zp))
#
#plt.figure()
#plt.plot(np.real(zm), np.imag(zm))


print("create a unit circle with specified center point")
x0 = 0.1
y0 = 0.05
R = 1.0- np.hypot(x0,y0)

tht = np.linspace(0, 2*np.pi, 500)
xx = R*np.cos(tht)+x0
yy = R*np.sin(tht)+y0

xx_ref = np.cos(tht)
yy_ref = np.sin(tht)

zeta = xx + 1j*yy

plt.figure()
plt.plot(np.real(zeta), np.imag(zeta), 'b-', xx_ref, yy_ref,'r-', x0, y0, 'b+', 0, 0, 'r+')
plt.axis('equal')
plt.xlabel('real(zeta)')
plt.ylabel('imag(zeta)')
plt.grid()
plt.show()

z = zeta + 1/zeta

plt.figure()
plt.plot(np.real(z), np.imag(z))
plt.axis('equal')
plt.xlabel('real(z)')
plt.ylabel('imag(z)')
plt.grid()
plt.show()

## test foil.py interpolation

points_nom = foil.Foil(uiuc_filename='clarky', interpolate=False)
points_coarse = foil.Foil(uiuc_filename='clarky', interpolate=10)
points_fine = foil.Foil(uiuc_filename='clarky', interpolate=True, smoothing=1e-6)

print(points_fine)

plt.figure()
plt.plot(points_fine[:,0], points_fine[:,1], 'k-')
plt.plot(points_nom[:,0], points_nom[:,1], 'r.')
plt.plot(points_coarse[:,0], points_coarse[:,1], 'b.')
plt.axis('equal')
plt.grid()

plt.xlim([-0.02,0.03])
plt.ylim([-0.01,0.01])

plt.savefig('foil_zoom.png', dpi=600, facecolor='w', edgecolor='w',
        orientation='landscape',
        transparent=False, bbox_inches=None, pad_inches=0.1,
        frameon=None)

#plt.xlim([-0.05,1.05])
#
#plt.savefig('foil.png', dpi=600, facecolor='w', edgecolor='w',
#        orientation='landscape',
#        transparent=False, bbox_inches=None, pad_inches=0.1,
#        frameon=None)

plt.show()

s = np.hstack(([0.], np.cumsum(np.hypot(np.diff(points_fine[:,0]), np.diff(points_fine[:,1])))))

dxds_0 = (points_fine[1,0] - points_fine[0,0])/(s[1]-s[0])
dxds_trap = (points_fine[2:,0] - points_fine[:-2,0])/(s[2:]-s[:-2])
dxds_1 = (points_fine[-1,0] - points_fine[-2,0])/(s[-1]-s[-2])

dyds_0 = (points_fine[1,1] - points_fine[0,1])/(s[1]-s[0])
dyds_trap = (points_fine[2:,1] - points_fine[:-2,1])/(s[2:]-s[:-2])
dyds_1 = (points_fine[-1,1] - points_fine[-2,1])/(s[-1]-s[-2])


dxds=np.hstack(([dxds_0], dxds_trap, [dxds_1]))
dyds=np.hstack(([dyds_0], dyds_trap, [dyds_1]))

ngrad = np.hypot(dxds, dyds)

dxds=dxds/ngrad
dyds=dyds/ngrad

plt.figure()
plt.plot(s, dxds)
plt.xlabel('s')
plt.ylabel('dxds_fine')
plt.grid()
plt.show()

plt.figure()
plt.plot(s, dyds)
plt.xlabel('s')
plt.ylabel('dyds_fine')
plt.grid()
plt.show()

