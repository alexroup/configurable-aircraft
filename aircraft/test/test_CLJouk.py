# -*- coding: utf-8 -*-
"""
Created on Sat Mar 17 17:51:03 2018

@author: Alex
"""

import numpy as np
import matplotlib.pyplot as plt

from CLJouk import CLJouk

def test_CLJouk():
    
    npts = 10000
    
    B = CLJouk()
    
    B.read('fuse_a0.csv')

    phi = np.linspace(0., np.pi, npts)
    x, y = B(phi)
    r = B.r(phi)

    fig1 = plt.figure()
    ax1 = fig1.gca()
    ax1.plot(x, y, '-')
    ax1.plot(B.x0, B.y0, 'r.')

    plt.ylabel('y')
    plt.xlabel('x')
    plt.axis('equal')
    plt.grid()

    fig2 = plt.figure()
    ax2 = fig2.gca()
    ax2.plot(phi, r, '-')

    plt.ylabel('r')
    plt.xlabel('phi')
    plt.grid()

#    fig2 = plt.figure()
#    ax2 = fig2.gca()
#    ax2.plot(s, B.K(s), '-')
#    ax2.plot(B.x0, B.K0, 'r.')
#
#    plt.ylabel('K')
#    plt.xlabel('s')
##    plt.ylim([-10,10])
#    plt.grid()
#
#    fig3 = plt.figure()
#    ax3 = fig3.gca()
#    ax3.plot(s, B.theta(s), '-')
#    ax3.plot(B.x0, B.theta0, 'r.')
#    
#    plt.ylabel('theta')
#    plt.xlabel('s')
##    plt.ylim([-4,4])
#    plt.grid()

    plt.draw()
    plt.show()
   