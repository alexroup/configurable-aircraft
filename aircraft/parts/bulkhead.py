# -*- coding: utf-8 -*-

import solid
import numpy as np

from scipy.special import erf
from geomdl import NURBS
from geomdl import utilities
from geomdl.visualization import VisMPL
import matplotlib.pyplot as plt

npts = 200

def make(a, b, n, thickness, flange_width, web_depth, output_filename='bulkhead.scad'):

    points = super_ellipse_ish(a, b, n)
    
    p = solid.polygon(points.tolist())
    
    p_interior = solid.offset(r = -web_depth)(p)
    
    p_interior = round_polygon(r = web_depth/2, p = p_interior)
    
    s1 = solid.linear_extrude(thickness) (p - p_interior)
    
    s2 = solid.linear_extrude(flange_width) (p - solid.offset(r = -thickness)(p))
    
    s = s1 + s2
    
    solid.scad_render_to_file(s, output_filename, file_header='$fn = 50;')

# C1 interpolate parameters between lower and upper value.
# Special handling to ensure parameters are strictly non decreasing as 
# abs(sin(theta)) increases.  This avoids sharp corners appearing due to
# the interpolation.
# INTERPOLATION IS DISABLED - results in curvature discontinuity at theta==0
# but avoids upper and lower halves from interfering in each other's curves.
def param_interp(theta, p):
    
    if len(p)>1:

        theta = np.arcsin(np.sin(theta))

        if p[0] < p[1]:
            
            t0 = 0.
            t1 = 0.

        elif p[0] > p[1]:
            
            t0 = 0.
            t1 = 0.
            
        else:
            
            t0 = -np.pi/2.
            t1 = np.pi/2.
        
        val = np.zeros(np.shape(theta))
        
        ii0 = theta <= t0
        ii1 = theta >= t1
        ii2 = np.logical_not(np.logical_or(ii0, ii1))
        
        val[ii0] = p[0]
        val[ii1] = p[1]
        
        x = 2.0*(np.sin(theta[ii2])-np.sin(t0)) / (np.sin(t1)-np.sin(t0)) - 1.
        
        val[ii2] = p[0] + (p[1] - p[0])*(np.sin(np.pi/2.*x) + 1.)/2.
        
    else:
        val = p
    
    return val

def super_ellipse_ish(a, b, n):
    
    theta = np.linspace(-np.pi, np.pi, npts)

    a_vec = param_interp(theta, a)
    n_vec = param_interp(theta, n)
    
    x = a_vec * np.sign(np.sin(theta)) * np.abs(np.sin(theta))**(2./n_vec)
    
    y = b * np.sign(np.cos(theta)) * np.abs(np.cos(theta))**(2./n_vec)

    points = np.vstack((x,y)).transpose()
#    points = np.vstack((points, points[0]))
    
    return points

def spline_profile(a, b, n):
    
    if np.size(a) == 1:
        a = [a, a]
        
    if np.size(n) == 1:
        n = [n, n]

    eta = 0.52

#    x = np.array([-a[0], -a[0], -a[0], -a[0]/4., 0., a[1]/4., a[1], a[1], a[1]])
#    y = np.array([0., b/4., b, b, b, b, b, b/4., 0.])
#    w = np.array([1., np.sqrt(n[0])/10., np.sqrt(n[0])/10., np.sqrt(n[0])/10., 1., np.sqrt(n[1])/10., np.sqrt(n[1])/10., np.sqrt(n[1])/10., 1.])

#    x = np.array([-a[0], -a[0], -eta*a[0], 0., eta*a[1], a[1], a[1]])
#    y = np.array([0., eta*b, b, b, b, eta*b, 0.])
#    w = np.array([1., n[0]/2., n[0]/2.,1., n[1]/2., n[1]/2., 1.])

#    th = np.linspace(0, np.pi/2., 5)
#    r0  = 1/np.cos(th[1])
#    r1 = 1.10
#    
#    print('r0 =', r0)
#    print('r1 =', r1)
#    
#    x = np.hstack((0., r0*np.sin(th[1]), r1*np.sin(th[2]), r0*np.sin(th[3]), a[1]))
#    y = np.hstack((1., r0*np.cos(th[1]), r1*np.cos(th[2]), r0*np.cos(th[3]), 0.))
#    w = np.array([1., 1., 1., 1., 1.])

    dth = 0.5/9

    th = np.array([0.0, dth, 0.25, 0.5 - dth, 0.5, 0.5 + dth, 0.75, 1. - dth, 1.0])*np.pi - np.pi/2.
#    th = np.linspace(-np.pi/2., np.pi/2., 9)
    r0  = 1./np.cos(dth*np.pi)
    r1 = np.sqrt(2.)
    
    print('r0 =', r0)
    print('r1 =', r1)
    
    x = np.hstack((-a[0], r0*np.sin(th[1]), r1*np.sin(th[2]), r0*np.sin(th[3]), 0., r0*np.sin(th[5]), r1*np.sin(th[6]), r0*np.sin(th[7]), a[1]))
    y = np.hstack((0., r0*np.cos(th[1]), r1*np.cos(th[2]), r0*np.cos(th[3]), 1., r0*np.cos(th[5]), r1*np.cos(th[6]), r0*np.cos(th[7]), 0.))
    w = np.array([1., 1., 1./np.sqrt(2.), 1., 1., 1., 1./np.sqrt(2.), 1., 1.])

    cptw = np.vstack((x*w, y*w, w)).transpose().tolist()
    
    print('cptw =', cptw)
    
    curve = NURBS.Curve()
    curve.delta = 1./(npts-1)
    curve.degree = 4
    curve.ctrlpts = cptw

#    curve.knotvector = utilities.generate_knot_vector(curve.degree, np.size(x))

    curve.knotvector = [0.0, 0.0, 0.0, 0.0, 0.0, 0.2, 0.4, 0.6, 0.8, 1.0, 1.0, 1.0, 1.0, 1.0]
    print('curve.knotvector =', curve.knotvector)
    curve.evaluate()

    points = np.array(curve.curvepts)
    
    th = np.linspace(-np.pi/2., np.pi/2, 1000)
    
    plt.figure()
    plt.plot(points[:,1], points[:,0], np.cos(th), np.sin(th))
    plt.axis('equal')
    plt.grid()

    vis_config = VisMPL.VisConfig(figure_size=[20, 15])
    vis_comp = VisMPL.VisCurve2D(config=vis_config)
    curve.vis = vis_comp
    curve.render()

    return points

def round_polygon(r, p):

    return solid.offset(r=r) (solid.offset(r=-r)(p))

