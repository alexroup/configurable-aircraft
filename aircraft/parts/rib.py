# -*- coding: utf-8 -*-

from foil2scad import Polygon
import solid
import os
from foil import Foil, Upper, Lower
import numpy as np
from scipy.interpolate import interp1d

def make(uiuc_filename, chord, thickness, spar_diam, 
         spar_x, spar_y, flange_width, web_depth, struts=[]):
    
    airfoil_name = os.path.splitext(os.path.basename(uiuc_filename))[0]
    output_filename = "%s.scad" % airfoil_name
    
    # airfoil polygon
    
    f = solid.scale(chord) (
            Polygon(uiuc_filename = uiuc_filename)    
        )

    # web

    web_radius = 0.5*web_depth
    

    spar_circles = solid.translate()
    for x,y,d in zip(spar_x, spar_y, spar_diam):
        
        tmp = solid.translate([x*chord, y*chord,0])(solid.circle(d/2))
        
        spar_circles = spar_circles + tmp
        
    foil = Foil(uiuc_filename = uiuc_filename)
    
    upper = Upper(foil)
    lower = Lower(foil)
    
    # remove any points where dx <= 0
    
    ii = np.diff(upper[:,0]) == 0.
    upper = np.delete(upper, ii.nonzero(), axis=0)
    
    ii = np.diff(lower[:,0]) == 0.
    lower = np.delete(lower, ii.nonzero(), axis=0)
    
    upper_interp = interp1d(upper[:,0], upper[:,1], kind='cubic')
    lower_interp = interp1d(lower[:,0], lower[:,1], kind='cubic')

    strut_mask = solid.union()
    for st in struts:
        
        y_u = upper_interp(st[0])
        y_l = lower_interp(st[1])
        
        dy = y_u - y_l
        dx = st[1] - st[0]
        
        d = np.sqrt(dx*dx + dy*dy)/dy*web_depth*0.5
        
        x_u0 = st[0]*chord - d
        y_u0 = upper_interp(x_u0/chord)*chord
        
        x_u1 = st[0]*chord + d
        y_u1 = upper_interp(x_u1/chord)*chord
        
        x_l0 = st[1]*chord - d
        y_l0 = lower_interp(x_l0/chord)*chord
        
        x_l1 = st[1]*chord + d
        y_l1 = lower_interp(x_l1/chord)*chord
        
        strut_points = [[x_u0, y_u0], [x_u1, y_u1], [x_l1, y_l1], [x_l0, y_l0], [x_u0, y_u0]]
        
        strut_mask = strut_mask + solid.polygon(strut_points)

    f_holes = round_polygon(r=web_radius, \
            p = solid.offset(r=-web_depth)(f) \
            - solid.offset(r=web_depth) (spar_circles) 
            - strut_mask)
        
    s1 = solid.linear_extrude(thickness) (
        f-f_holes
    )
    
    # outer flange
    s2 = solid.linear_extrude(flange_width) (
        f - solid.offset(r=-thickness) (f)
    )

    # spar flange
    s3 = solid.linear_extrude(flange_width) (
        solid.offset(r=thickness)(spar_circles) - spar_circles
    )
        
    # union
    s4 = s1 + s2 + s3
    
    # cut outer perimeter
    s5 = solid.linear_extrude(flange_width) (f)

    s6 = s4 * s5
    
    # cut spar interior
    s7 = solid.linear_extrude(4*flange_width, center = True)(spar_circles)

    s = s6 - s7
    
    solid.scad_render_to_file(s, output_filename, file_header='$fn = 50;')

def round_polygon(r, p):

    return solid.offset(r=r) (solid.offset(r=-r)(p))
