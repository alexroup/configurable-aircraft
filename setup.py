# -*- coding: utf-8 -*-
import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="aircraft",
    version="0.0.1",
    author="Alex Roup",
    author_email="alex@avraero.com",
    description="A small example package",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/alexroup/configurable-aircraft/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GPLv3",
        "Operating System :: OS Independent",
    ],
)
