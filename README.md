# README #

```
rib.make('clarky', chord=200., thickness=1., spar_diam=[4., 10.,4.], 
	spar_x=[0.008,0.25,0.94], spar_y=[-0.0025,0.03,0.0065], 
	flange_width=3., web_depth=3., 
	struts=[[0.15,0.1],[0.45,0.3],[0.45,0.6],[0.75,0.6]])
	
planar_surface.make(100,100,50,35,50,2,8,num_ribs=1,r=20,
	single_surface=False,output_filename='planar_surface.scad')

bulkhead.make([100.,200.],100.,[3., 2.5],thickness=3., flange_width=10, 
	web_depth=40.)
```
![example clark-y rib image](https://bitbucket.org/alexroup/airfoil-utils/raw/cbc0bea56d01cdc5ebd060b44f0c6a0ad04e33a6/media/example3.png)

![example clark-y rib image](https://bitbucket.org/alexroup/airfoil-utils/raw/cbc0bea56d01cdc5ebd060b44f0c6a0ad04e33a6/media/print_2.jpg)

![example clark-y rib image](https://bitbucket.org/alexroup/airfoil-utils/raw/cbc0bea56d01cdc5ebd060b44f0c6a0ad04e33a6/media/print_1.jpg)

![example planar_surface image](https://bitbucket.org/alexroup/airfoil-utils/raw/cbc0bea56d01cdc5ebd060b44f0c6a0ad04e33a6/media/planar_surface.png)

![example bulkhead image](https://bitbucket.org/alexroup/airfoil-utils/raw/cbc0bea56d01cdc5ebd060b44f0c6a0ad04e33a6/media/bulkhead.png)

pip install --no-index --find-links=. aircraft
